{$I head}

UNIT GenericProcess;
(* this unit is for placing all the generic utility functions and procedures in
as this cleans up the code of all future applications so as to not get messy *)

INTERFACE

CONST
        copyright = '2018';
        author = 'Simon Jackson, K Ring Technologies Ltd.';
        numVerbs = 7;
        numOlderVerbs = 1;
{$IFDEF WINDOWS}
        progName = 'clifly.exe'; (* for some reason the windows goes funny *)
{$ELSE}
{$IFDEF HASAMIGA}
        progName = 'CLIFly';
{$ELSE}
        progName = 'clifly';
{$ENDIF}
{$ENDIF}
        numVersions = 2;

        (* some string literal usefuls *)
        tb = #9;
        br = LineEnding; (* a short form line break *)

TYPE
        verLimit = 1 .. numVersions;
        verbCount = 1 .. (numVerbs + numOlderVerbs);
        ExeRec = RECORD
                        v: String; (* verb *)
                        f: verLimit; (* version included from via index *)
                        p: PROCEDURE(); (* procedure to do *)
                        t: FUNCTION(): Boolean; (* test function *)
                        a: String; (* argument usage string *)
                        x: ^ExeRecArray; (* possible sub verb arrangement *)
                        h: String; (* extended help text *)
        END;
        ExeRecArray = Array of ExeRec;
        PExe = ^ExeRecArray;
        MainTable = Array [verbCount] of ExeRec;

CONST
        version: Array [verLimit] of String = ('0.0.0', '0.0.1');

TYPE
        procParam = PROCEDURE();

FUNCTION getParse(): String;
FUNCTION hasParse(): Boolean;
FUNCTION getIntParse(): Integer;

FUNCTION wrapper(this, that: procParam): Boolean; (* function call with exception boolean *)

PROCEDURE ExitOK(); (* will actually warn if there is an accumulated warning *)
PROCEDURE ExitWarn(plus: String);
PROCEDURE ExitError(plus: String);
PROCEDURE ExitFail(plus: String);

PROCEDURE ContinueOK(); (* clears warning accumulation, on resolving issue *)
PROCEDURE ContinueWarn(plus: String);

FUNCTION nullTest(): Boolean;
PROCEDURE nullProc();

CONST
        nullHelp = 'There is no extended help for this verb.';

PROCEDURE printUsage();
PROCEDURE printVersion();
PROCEDURE checkVersion();
PROCEDURE hasVersion();
PROCEDURE tests();
PROCEDURE testOne();
FUNCTION testNest(input: PExe): Boolean;
PROCEDURE older();
PROCEDURE start(verbSet: PExe);

IMPLEMENTATION

USES SysUtils, HardException, HardFile, Help;

VAR
        WarnCheck : Boolean = False;
        verbs: ExeRecArray;
        verbsChk: PExe;
        counter: Integer = 1; (* first parameter *)
        currentVerb: String;

FUNCTION getParse(): String;
BEGIN
        IF ParamCount < counter THEN
                BEGIN
                        Write();
                        ExitError(currentVerb + ' needs more arguments.');
                END
        ELSE
                getParse := ParamStr(counter);
        counter := counter + 1;
END;

FUNCTION hasParse(): Boolean;
BEGIN
        hasParse := (counter <= ParamCount);
END;

FUNCTION getIntParse(): Integer;
VAR
        s: String;
BEGIN
        s := getParse();
        TRY
                getIntParse := StrToInt(s);
        EXCEPT
                ON EConvertError DO
                        ExitError(currentVerb + ' needs a number. "' + s + '" is not a number.');
        END;
END;

(* you can always use a proxy for @ExitFail or other things for that *)
FUNCTION wrapper(this, that: procParam): Boolean;
BEGIN
        TRY
                wrapper := True;
                this();
        EXCEPT
                ON E: AppException DO
                        BEGIN
                                wrapper := False;
                                ContinueWarn(E.Message);
                                TRY
                                        that(); (* warn state *)
                                EXCEPT
                                        ON F: AppException DO
                                                BEGIN
                                                        F[FatalEx] := E;
                                                        ExitError(F.Message);
                                                END;
                                        ON F: Exception DO (* case of unhandled exception *)
                                                BEGIN
                                                        ExitFail(F.Message);
                                                END;
                                END;
                        END;
                ON E: Exception DO (* case of unhandled exception *)
                        BEGIN
                                ExitFail(E.Message);
                        END;
        END;
END;

PROCEDURE ExitOK();
BEGIN
        IF WarnCheck THEN ExitWarn('A previous warning was issued.');
        Halt(0);
END;

PROCEDURE ExitWarn(plus: String);
BEGIN
        makeLog(progName + ' exited with a warning. ' + plus);
        Write(plus, br);
        Halt(5);
END;

PROCEDURE ExitError(plus: String);
BEGIN
        makeLog(progName + ' exited with an error. ' + plus);
        Write(plus, br);
        Halt(10);
END;

PROCEDURE ExitFail(plus: String);
BEGIN
        makeLog(progName + ' exited with a failure. An individual test fail or unanticipated exception happened. ' + plus);
        Write(plus, br);
        Halt(20);
END;

PROCEDURE ContinueOK();
BEGIN
        WarnCheck := False;
END;

PROCEDURE ContinueWarn(plus: String);
BEGIN
        makeLog(progName + ' raised a warning. ' + plus);
        Write(plus, br);
        WarnCheck := True;
END;

FUNCTION nullTest(): Boolean;
BEGIN
        Write('No test has been written.', br);
        nullTest := True; (* OK *)
END;

PROCEDURE nullProc();
BEGIN
        Write('There is nothing older than this. The "old NULL terminal" to prevent recursion parse overflow.', br,
                'Why did recursion cross the road? ', progName, ' older 0.0.0 older', br);
        (* nothing here *)
END;

FUNCTION testFromId(id: Integer): Boolean;
BEGIN
        testFromId := verbs[id].t();
        IF testFromId THEN
                BEGIN
                        Write(verbs[id].v, ' tested OK.', br);
                END
        ELSE
                BEGIN
                        continueWarn(verbs[id].v + ' tested FAIL.');
                END;
END;

PROCEDURE testOne();
VAR
        arg: Integer;
        ver: String;
BEGIN
        ver := getParse();
        FOR arg := Low(verbs) TO High(verbs) DO
                IF CS(verbs[arg].v, ver) THEN
                        IF testFromId(arg) THEN
                                ExitFail(ver + ' tested FAIL.'); (* might be useful to abort *)
END;

PROCEDURE tests();
VAR
        arg: Integer;
        count: Integer = 0;
BEGIN
        FOR arg := Low(verbs) TO High(verbs) DO
                IF testFromId(arg) THEN
                        count := count + 1;
        IF count = (High(verbs) - Low(verbs) + 1) THEN
                Write('All tests pass.', br)
        ELSE
                ExitWarn('Only ' + IntToStr(count) + ' tests passed. There is a problem.');
END;

FUNCTION testNest(input: PExe): Boolean;
VAR
        arg: Integer;
        save: PExe;
BEGIN
        testNest := True;
        save := @verbs;
        verbs := input^;
        FOR arg := Low(verbs) TO High(verbs) DO
                IF not(testFromId(arg)) THEN
                        testNest := False;
        verbs := save^; (* restore *)
END;

PROCEDURE hasVersion();
VAR
        arg: Integer;
        ver: String;
BEGIN
        ver := getParse();
        arg := Low(verbs);
        WHILE arg <= High(verbs) DO
                BEGIN
                        IF CS(verbs[arg].v, ver) THEN
                                IF (verbs[arg].x = verbsChk) or not(hasParse()) THEN
                                        BEGIN
                                                Write(version[verbs[arg].f], br);
                                                ContinueOK();
                                                EXIT;
                                        END
                                ELSE
                                        BEGIN
                                                verbs := verbs[arg].x^; (* assign the set *)
                                                verbsChk := verbs[arg].x;
                                                arg := Low(verbs); (* test done before step *)
                                                ver := getParse();
                                                CONTINUE;
                                        END;
                        arg := arg + 1;
                END;
        ContinueWarn('There is no version for the command.');
END;

PROCEDURE printVersion();
BEGIN
        Write(version[numVersions], br);
END;

PROCEDURE checkVersion();
VAR
        arg: verLimit;
        ver: String;
BEGIN
        ver := getParse();
        FOR arg := 1 TO numVersions DO
                IF CS(ver, version[arg]) THEN
                        BEGIN
                                ContinueOK();
                                EXIT;
                        END;
        ContinueWarn('There is no such version.');
END;

PROCEDURE printLast(l: Boolean);
BEGIN
        IF l THEN
                Write(']')
        ELSE
                Write('|');
END;

PROCEDURE printNested(arg: Integer; verbs: PExe);
VAR
        count: Integer;
BEGIN
        IF verbs^[arg].x = verbs THEN (* the null terminal is the list itself - self contained *)
                Write(verbs^[arg].a)
        ELSE
                BEGIN
                        Write('[');
                        FOR count := Low(verbs^) TO High(verbs^) DO
                                BEGIN
                                        Write(verbs^[count].v, ' ');
                                        printNested(count, verbs^[count].x);
                                        printLast(count = High(verbs^));
                                END;
                END;
END;

PROCEDURE printUsage();
VAR
        arg: Integer;
        nam: String;
BEGIN
        Write(progName, ' version ', version[numVersions], '(C)', copyright, ' ', author, br);
        Write('Usage: ', progName, ' ');
        IF hasParse() THEN
                BEGIN
                        (* single command *)
                        nam := getParse();
                        arg := Low(verbs);
                        WHILE arg <= High(verbs) DO
                                BEGIN
                                        IF CS(verbs[arg].v, nam) THEN
                                                BEGIN
                                                        Write(verbs[arg].v, ' ');
                                                        IF (verbs[arg].x = verbsChk) or not(hasParse()) THEN
                                                                BEGIN
                                                                        printNested(arg, verbsChk);
                                                                        Write(' ; ', version[verbs[arg].f], br, br);
                                                                        Write('Help: ', verbs[arg].h, br);
                                                                        EXIT; (* help printed *)
                                                                END
                                                        ELSE (* not terminal *)
                                                                BEGIN
                                                                        verbs := verbs[arg].x^; (* assign the set *)
                                                                        verbsChk := verbs[arg].x;
                                                                        arg := Low(verbs); (* test done before step *)
                                                                        nam := getParse();
                                                                        CONTINUE;
                                                                END;
                                                        arg := arg + 1;
                                                END;
                                        END;
                        Write(nam, ' is an invalid verb name and so no usage help.', br);
                        EXIT;
                END;
        Write('[ ');
        FOR arg := 1 TO numVerbs DO
                BEGIN
                        Write(verbs[arg].v, ' ');
                        printNested(arg, verbsChk);
                        Write(' ; ', version[verbs[arg].f], '|', br);
                END;
        FOR arg := numVerbs + 1 TO numVerbs + numOlderVerbs DO
                BEGIN
                        Write('older ', version[verbs[arg].f], ' ', verbs[arg].v, ' ');
                        printNested(arg, verbsChk);
                        Write(' ; ', version[verbs[arg].f]);
                        printLast(arg = numVerbs + numOlderVerbs);
                        Write(br);
                END;
END;

PROCEDURE doVerb(atVer: verLimit);
VAR
        arg: Integer;
        verb: String;

BEGIN
        currentVerb := progName;
        IF not(hasParse()) THEN
        BEGIN
                Write(progName, ' has no verb following, so help assumed.', br);
                printUsage();
                ExitOK(); (* exit from program *)
        END;
        verb := getParse();
        FOR arg := Low(verbs) TO High(verbs) DO
                IF CS(verb, verbs[arg].v) and (atVer >= verbs[arg].f) THEN (* older things *)
                        BEGIN
                                IF verbs[arg].x <> verbsChk THEN
                                        start(verbs[arg].x) (* nest *)
                                ELSE
                                        BEGIN
                                                currentVerb := verbs[arg].v; (* store context global *)
                                                verbs[arg].p();
                                        END;
                                IF not(hasParse()) THEN
                                        ExitOK() (* return to system with no error code *)
                                ELSE
                                        BEGIN
                                                ExitWarn(verbs[arg].v + ' uses fewer arguments.');
                                        END;
                        END;
        ExitError(verb + ' is not a valid verb action.');
END;

PROCEDURE older();
CONST
        verNum: verLimit = 1; (* oldest *)
VAR
        ver: String;
        arg: verLimit;
BEGIN
        ver := getParse();
        FOR arg := 1 TO numVersions - 1 DO (* remove the nested version of latest to kill parse stack overflow *)
                IF CS(version[arg], ver) THEN
                        verNum := arg;
        doVerb(verNum); (* recursive *)
END;

PROCEDURE start(verbSet: PExe);
BEGIN
        verbs := verbSet^; (* assign the set *)
        verbsChk := verbSet;
        doVerb(numVersions);
END;

END.
