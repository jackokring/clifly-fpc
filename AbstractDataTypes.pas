{$I head}

UNIT AbstractDataTypes;
(* this unit is for placing all the useful abstract data types in
as this cleans up the code of all future applications so as to not get messy *)

INTERFACE

USES HardException;

TYPE
    GENERIC GListEl<T> = CLASS
        PROTECTED
            next: SPECIALIZE GListEl<T>;
            val: T;
    END;

    (* a dynamic is GListKeyEl<T> with hard cast, should have more performance from a instance pointer *)
    GENERIC GKey<T> = CLASS
        PUBLIC
            key: String;
            value: T;
            (* this ironic override makes for hash finding on other stuff possible *)
            FUNCTION ToString(): String; OVERRIDE;
    END;

    GENERIC GListIter<T> = CLASS
        PROTECTED TYPE
            El = SPECIALIZE GListEl<T>;
        PROTECTED VAR
            FCurrent: El;
            FLast: El; (* for handling delete() where need to know pointer from last *)
            FUNCTION GetCurrent(): T;
        PUBLIC
            CONSTRUCTOR Create();
            FUNCTION MoveNext: Boolean;
            PROPERTY Current: T READ GetCurrent;
    END;

    GENERIC GList<T> = CLASS
        PROTECTED TYPE
            (* this private type is required to get all the specialized tings to work *)
            El = SPECIALIZE GListEl<T>;
            Iterate = SPECIALIZE GListIter<T>;
            IterStack = SPECIALIZE GList<Iterate>; (* might need to check certain things (iteration stack) *)
            ChainSelf = SPECIALIZE GList<T>;
            pfKind = SPECIALIZE GList<El>;
            { Keyed = SPECIALIZE GListEl<GKey<T>>; (* many of these types are for easy coding without specialized in the implementation division *) }
        PROTECTED VAR
            first: El;
            last: El;
            (* the preFree list must not be (infinite) nested with the use of inner iterates *)
            preFree: pfKind; (* rules on freeing and iterates in use, and also append() is useful for eventual LIFO on postFree *)
            class postFree: El; (* this is a free list recycler management thing *)
            currentIterates: IterStack;
        PROTECTED
            FUNCTION make(): El; VIRTUAL;
            PROCEDURE recycle(input: El); VIRTUAL;
        PUBLIC
            (* many of these are not virtual due to the nature of generics *)
            (* OoO processors with branch prediction excel when not cache thrashing *)
            (* pointer chasing ruins the process, so plenty of cycles to check if statements invisibly *)
            (* many apparent sub types are best done as if tests on Booleans *)
            FUNCTION GetEnumerator(): Iterate;
            FUNCTION push(CONST input: T): ChainSelf;
            FUNCTION pop(): T;
            FUNCTION append(CONST input: T): ChainSelf;
            FUNCTION peek(): T;
            FUNCTION moon(): T;
            FUNCTION reflow(): ChainSelf;
            (* basic creation and destruction *)
            CONSTRUCTOR Create();
            DESTRUCTOR Destroy(); OVERRIDE;
            (* some extra functionality *)
            FUNCTION isEmpty(): Boolean;
            PROCEDURE delete();
            PROCEDURE update(input: T);
            PROCEDURE insertBefore(input: T);
            PROCEDURE insertAfter(input: T);
            PROCEDURE lowerPriority(); (* puts this iterator value after the next by a delete insertAfter thing *)
            PROCEDURE loopExit(); (* makes the inner most iterator in loop constructs end, so finishing the loop on this element *)
            PROCEDURE index(pos: LongInt); (* this may take a while :) *)
            (* this function is for use with named keys *)
            PROCEDURE key(named: String); (* hash equality *)
            (* this function destroys order, but speeds access on long keyed lists *)
            (* the difference between append and push becomes relevant only on hash collisions *)
            (* the number of pointer chase links causes reorder when it exceeds colChurn on recall *)
            PROCEDURE setPreHash(colChurn: Integer);
            (* this function keeps order, but trades space for quicker number indexing time *)
            PROCEDURE setPreIndex();
    END;

    (* for shallow copy regeneration *)
    FUNCTION regen(input: TObject): TObject; OVERLOAD;
    FUNCTION regen(input: Int64): Int64; OVERLOAD;
    FUNCTION regen(input: UInt64): UInt64; OVERLOAD;
    FUNCTION regen(input: String): String; OVERLOAD;

    PROCEDURE copyRegen(CONST input: Pointer; VAR output: Pointer, CONST size: LongInt);

IMPLEMENTATION

//==========================================================

FUNCTION GList.isEmpty(): Boolean;
BEGIN
    IF (first = Nil) xor (last = Nil) THEN
        isEmpty := (first = Nil)
    ELSE
        RAISE AppEx[FormatEx];
END;

CONSTRUCTOR GList.Create();
BEGIN
    TRY
        INHERITED;
        first := Nil;
        last := Nil;
    EXCEPT
        RAISE AppEx[NameEx]; (* most likely no space *)
    END;
END;

DESTRUCTOR GList.Destroy();
VAR
    v, w: El;
BEGIN
    IF not(isEmpty()) THEN
        BEGIN
            w := first;
            WHILE not(isEmpty()) DO
                BEGIN
                    IF (last.next <> Nil) THEN
                        RAISE AppEx[FormatEx]; (* should never happen, multi-threaded append? *)
                    v := first;
                    WHILE (last <> v) DO
                        v := v.next; (* time bounds check on completion => hang instead of massive delete *)
                    IF (first <> w) THEN
                        RAISE AppEx[FormatEx]; (* should never happen, multi-threaded push or pop? *)
                    pop();
                END;
        END;
    TRY
        INHERITED;
    EXCEPT
        RAISE AppEx[FinalEx];
    END;
END;

FUNCTION GList.push(CONST input: T): ChainSelf;
VAR
    v: El;
BEGIN
    v := make();
    v.val := T;
    v.next := Nil;
    TRY
        IF isEmpty() THEN (* list empty *)
            BEGIN
                first := v;
                last := v;
            END
        ELSE
            BEGIN
                v.next := first;
                first := v;
            END;
    EXCEPT
        recycle(v);
        RAISE;
    END;
    push := self;
END;

FUNCTION GList.pop(): T;
VAR
    v: El;
BEGIN
    IF isEmpty() THEN
        RAISE AppEx[ConsumerEx]
    ELSE
        BEGIN
            pop := first.val;
            v := first;
            IF (last.next <> Nil) THEN
                RAISE AppEx[FormatEx] (* should never happen *)
            ELSE
                BEGIN
                    first := first.next;
                    IF (first = Nil) THEN
                        last := Nil; (* empty signature *)
                    recycle(v); (* GC *)
                END;
        END;
END;

FUNCTION GList.append(CONST input: T): ChainSelf;
VAR
    v: El;
BEGIN
    v := make();
    v.val := T;
    v.next := Nil;
    TRY
        IF isEmpty() THEN
            BEGIN
                last := v;
                first := v;
            END
        ELSE IF (last.next <> Nil) THEN
            RAISE AppEx[FormatEx] (* a heap corruption perhaps? *)
        ELSE
            BEGIN
                last.next := v;
                last := v;
            END;
    EXCEPT
        recycle(v);
        RAISE;
    END;
    append := self;
END;

FUNCTION GList.peek(): T;
BEGIN
    IF not(isEmpty()) THEN
        peek := first.val
    ELSE
        RAISE AppEx[OriginEx]; (* no consumption *)
END;

FUNCTION GList.moon(): T;
BEGIN
    IF not(isEmpty()) THEN
        moon := last.val
    ELSE
        RAISE AppEx[OriginEx];
END;

FUNCTION GList.make(): El;
BEGIN
    TRY
        make := El.Create();
    EXCEPT
        RAISE AppEx[NameEx];
    END;
END;

PROCEDURE GList.recycle(input: El);
BEGIN
    TRY
        input.val := Nil; (* just good practice *)
        input.Free();
    EXCEPT
        RAISE AppEx[FinalEx];
    END;
END;

FUNCTION GList.reflow(): ChainSelf;
VAR
    v: El;
BEGIN
    IF not(isEmpty()) and (IterStack.isEmpty()) THEN
        BEGIN
            IF (last.next = Nil) THEN
                RAISE AppEx[FormatEx]; (* should never happen *)
            v := first;
            WHILE (first <> v) DO
                append(regen(pop())); (* churn the heap to free all elements *)
        END
    ELSE
        reflow := Nil; (* did not make new *)
END;

{$POINTERMATH ON}

PROCEDURE copyRegen(CONST input: Pointer; VAR output: Pointer, CONST size: LongInt);
VAR
    i: LongInt;
BEGIN
    FOR i := 0 TO size - 1 DO
        output[i] := input[i]; (* clone instance *)
END;

FUNCTION regen(input: TObject): TObject;
VAR
    j, k: PPByte;
BEGIN
    TRY
        regen := input.newInstance(); (* dynamic class type *)
        copyRegen(Pointer(input), Pointer(regen), v.instanceSize());
        input.Free;
    EXCEPT
        regen.Free;
        RAISE;
    END;
END;

FUNCTION regen(input: String): String;
BEGIN
    SetLength(regen, Length(input)); (* dynamic class type *)
    copyRegen(Pointer(input), Pointer(regen), Length(input));
END;

FUNCTION regen(input: Int64): Int64;
BEGIN
    (* botch numerics *)
    (* copyRegen(Pointer(@input), Pointer(@regen), SizeOf(regen)); *)
    regen := input;
END;

FUNCTION regen(input: UInt64): UInt64;
BEGIN
    regen := input;
END;

//===============================================================

FUNCTION GKey.ToString(): String;
BEGIN
    (* make string depend on key only *)
    ToString := key;
END;

END.
