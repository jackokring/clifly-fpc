{#####################################################################################}
{##                                                                                 ##}
{## ABC - Advanced Blocksorting Compressor                                          ##}
{##                                                                                 ##}
{## ABC_BWT_Unit                                                                    ##}
{##                                                                                 ##}
{## Blocksorting with the Burrows-Wheeler transformation                            ##}
{##                                                                                 ##}
{## Copyright (C) 2002-2003 J�rgen Abel                                             ##}
{##                                                                                 ##}
{#####################################################################################}
{$MODE DELPHI}

{$I ABC_Include.pas}

Unit ABC_BWT_Unit;


Interface


Uses
  ABC_Global_Unit;


Type
  T_ABC_BWT = Class
  Protected
    M_P_Input_Buffer  : T_P_ABC_Byte_Array;         { Pointer to input data }
    M_P_Output_Buffer : T_P_ABC_Byte_Array;         { Pointer to output data }
    M_Block_Size      : Integer;                    { Size of input data }
    M_P_A_I           : T_P_ABC_Integer_Array;      { Pointer to sort array }
    M_EOF_Index       : Integer;                    { Index of EOF }
    {$IFDEF OPTIMIZATION_ON}
    {$ELSE}
    Function    Greater (L : Integer; R : Integer) : Boolean;
    {$ENDIF}
    Procedure   Shell_Sort (L : Integer; R : Integer; Depth : Integer);
    Function    Get_Pivot (L : Integer; R : Integer; Depth : Integer) : Byte;
    Procedure   Ternary_Quicksort (L : Integer; R : Integer; Depth : Integer);
    Procedure   Suffix_Sort (Var Compress_Information : T_R_ABC_Compress_Information);
  Public
    Procedure   Encode (Var Compress_Information : T_R_ABC_Compress_Information);
    Procedure   Decode (Var Compress_Information : T_R_ABC_Compress_Information);
  End; { T_ABC_BWT }


Implementation


(* Uses
  ABC_MCP_UNit; *)


Const
  WINARX_BWT_EOF                          = 0;              { Symbol for EOF }
  WINARX_BWT_BLOCK_SORTED                 = 1;              { Block size of totally sorted block }
  WINARX_BWT_MEDIAN_THRESHOLD             = 256;            { Threshold for pivot }
  WINARX_BWT_SHELL_RANGE_THRESHOLD        = 12;             { Threshold for using shell-sort }
  WINARX_BWT_SHELL_SORT_DEPTH_THRESHOLD   = 24;             { Threshold for depth, using shell-sort }
  WINARX_BWT_QUICKSORT_STACK_SIZE         = 128;            { Stack size for Quicksort }
  WINARX_BWT_EOF_BUFFER_SIZE              = 64;             { Savety buffer for EOF }


{$IFDEF OPTIMIZATION_ON}
{$ELSE}
{-------------------------------------------------------------------------------------}
Function T_ABC_BWT.Greater (L : Integer; R : Integer) : Boolean; Register;
{ Returns true, if string starting at L is greater than string starting at R          }
{-------------------------------------------------------------------------------------}
  Var
    Symbol_L : Byte;
    Symbol_R : Byte;

  Begin { T_ABC_BWT.Greater }
    Result    := TRUE;

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    Symbol_L  := M_P_Input_Buffer^ [L];
    Symbol_R  := M_P_Input_Buffer^ [R];
    If Symbol_L <> Symbol_R then
      Begin { then }
        If Symbol_L < Symbol_R then
          Begin { then }
            Result := FALSE;
          End; { then }
        Exit;
      End; { then }
    Inc (L);
    Inc (R);

    While TRUE do
      Begin { While }
        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);

        Symbol_L  := M_P_Input_Buffer^ [L];
        Symbol_R  := M_P_Input_Buffer^ [R];
        If Symbol_L <> Symbol_R then
          Begin { then }
            If Symbol_L < Symbol_R then
              Begin { then }
                Result := FALSE;
              End; { then }
            Exit;
          End; { then }
        Inc (L);
        Inc (R);
      End; { While }
  End; { T_ABC_BWT.Greater }
{$ENDIF}


{-------------------------------------------------------------------------------------}
Procedure T_ABC_BWT.Shell_Sort (L : Integer; R : Integer; Depth : Integer); Register;
{ Sorting of smaller arrays                                                           }
{-------------------------------------------------------------------------------------}
  Const
    A_Increment : Array [0 .. 13] of Integer = (1, 4, 13, 40, 121, 364, 1093, 3280, 9841, 29524, 88573, 265720, 797161, 2391484);

  Var
    D               : Integer;
    Increment_Index : Integer;
    H               : Integer;
    I               : Integer;
    J               : Integer;
    V               : Integer;
    {$IFDEF OPTIMIZATION_ON}
    B_Greater       : Boolean;
    P_Symbol_0      : Pointer;
    P_Symbol_1      : Pointer;
    P_EOF           : Pointer;
    {$ELSE}
    {$ENDIF}

  Begin { T_ABC_BWT.Shell_Sort }
    { Calculate start index }
    D               := R - L + 1;
    Increment_Index := 0;
    While A_Increment [Increment_Index] < D do
      Begin { While }
        Inc (Increment_Index);
      End; { While }
    Dec (Increment_Index);

    { Loop all Increments }
    While Increment_Index >= 0 do
      Begin { While }
        H := A_Increment [Increment_Index];
        I := L + H;

        { Handle one increment }
        While TRUE do
          Begin { While }
            If I > R then
              Begin { then }
                Break;
              End; { then }

            V := M_P_A_I^ [I];
            J := I;

            {$IFDEF OPTIMIZATION_ON}
            While TRUE do
              Begin { While }
                B_Greater   := TRUE;
                P_Symbol_0  := @M_P_Input_Buffer^ [M_P_A_I^ [J - H] + Depth];
                P_Symbol_1  := @M_P_Input_Buffer^ [V + Depth];
                P_EOF       := @M_P_Input_Buffer^ [M_Block_Size + WINARX_BWT_EOF_BUFFER_SIZE];
                Asm
                  PUSH    EDI
                  PUSH    ESI
                  MOV     ESI,[P_Symbol_0]
                  MOV     EDI,[P_Symbol_1]

@_Shell_Sort_10:
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12
                  MOV     EAX,[ESI]
                  ADD     ESI,4
                  BSWAP   EAX
                  MOV     EDX,[EDI]
                  ADD     EDI,4
                  BSWAP   EDX
                  CMP     EAX,EDX
                  JNZ     @_Shell_Sort_12

                  CMP     ESI,[P_EOF]       { Test, if EOF is reached }
                  JNC     @_Shell_Sort_11
                  CMP     EDI,[P_EOF]
                  JS      @_Shell_Sort_10
@_Shell_Sort_11:
                  CMP     EDI,ESI

@_Shell_Sort_12:
                  JNC     @_Shell_Sort_13
                  MOV     [B_Greater],FALSE

@_Shell_Sort_13:
                  POP     ESI
                  POP     EDI
                End; { Asm }

                If B_Greater = FALSE then
                  Begin { then }
                    Break;
                  End; { then }

            {$ELSE}
            While Greater (M_P_A_I^ [J - H] + Depth, V + Depth) = TRUE do
              Begin { While }
            {$ENDIF}
                M_P_A_I^ [J] := M_P_A_I^ [J - H];
                Dec (J, H);
                If J <= L + H - 1 then
                  Begin { then }
                    Break;
                  End; { then }
              End; { While }
            M_P_A_I^ [J] := V;
            Inc (I);
          End; { While }

        Dec (Increment_Index);
      End; { While }
  End; { T_ABC_BWT.Shell_Sort }


{-------------------------------------------------------------------------------------}
Function T_ABC_BWT.Get_Pivot (L : Integer; R : Integer; Depth : Integer) : Byte;
{ Calculating the middle element of an array                                          }
{-------------------------------------------------------------------------------------}
  {$IFDEF OPTIMIZATION_ON}
  Var
    M               : Integer;
    D               : Integer;
    T_0             : Integer;
    T_1             : Integer;
    T_2             : Integer;
    P_A_I           : Pointer;
    P_Input_Buffer  : Pointer;

  Begin { T_ABC_BWT.Get_Pivot }
    P_A_I           := M_P_A_I;
    P_Input_Buffer  := M_P_Input_Buffer;
    Asm
      PUSH    EBX
      PUSH    ESI
      PUSH    EDI

      MOV     ESI,[P_A_I]
      MOV     EDI,[P_Input_Buffer]

      MOV     EAX,[L]
      MOV     EBX,[R]
      MOV     ECX,EAX
      ADD     ECX,EBX
      SHR     ECX,1
      MOV     [M],ECX
      MOV     ECX,EBX
      SUB     ECX,EAX
      CMP     ECX,WINARX_BWT_MEDIAN_THRESHOLD
      JC      @_Get_Pivot_10

      MOV     EDX,EBX
      SUB     EDX,EAX
      SHR     EDX,3
      MOV     [D],EDX

      MOV     [T_0],EAX
      ADD     EAX,[D]
      MOV     [T_1],EAX
      ADD     EAX,[D]
      MOV     [T_2],EAX
      CALL    @_Get_Pivot_20
      MOV     [L],EAX

      MOV     EAX,[M]
      MOV     EBX,[D]
      MOV     ECX,EAX
      SUB     ECX,EBX
      MOV     [T_0],ECX
      MOV     [T_1],EAX
      ADD     EAX,EBX
      MOV     [T_2],EAX
      CALL    @_Get_Pivot_20
      MOV     [M],EAX

      MOV     EAX,[R]
      MOV     EBX,[D]
      MOV     ECX,EAX
      SUB     ECX,EBX
      MOV     [T_1],ECX
      SUB     ECX,EBX
      MOV     [T_0],ECX
      MOV     [T_2],EAX
      CALL    @_Get_Pivot_20
      MOV     [M],EAX

@_Get_Pivot_10:
      MOV     EAX,[L]
      MOV     [T_0],EAX
      MOV     EAX,[M]
      MOV     [T_1],EAX
      MOV     EAX,[R]
      MOV     [T_2],EAX
      CALL    @_Get_Pivot_20
      MOV     EAX,[ESI + EAX*4]
      ADD     EAX,[Depth]
      MOV     AL,[EDI + EAX]
      MOV     [Result],AL

      POP     EDI
      POP     ESI
      POP     EBX
      JMP     @_Get_Pivot_90

@_Get_Pivot_20:
      MOV     EAX,[T_0]
      MOV     EAX,[ESI + EAX*4]
      ADD     EAX,[Depth]
      MOV     AL,[EDI + EAX]

      MOV     EBX,[T_1]
      MOV     EBX,[ESI + EBX*4]
      ADD     EBX,[Depth]
      MOV     BL,[EDI + EBX]

      MOV     ECX,[T_2]
      MOV     ECX,[ESI + ECX*4]
      ADD     ECX,[Depth]
      MOV     CL,[EDI + ECX]

      CMP     BL,CL
      JNA     @_Get_Pivot_60
      CMP     AL,BL
      JNA     @_Get_Pivot_50
      MOV     EAX,[T_1]
      JMP     @_Get_Pivot_80
@_Get_Pivot_50:
      CMP     AL,CL
      JNA     @_Get_Pivot_55
      MOV     EAX,[T_0]
      JMP     @_Get_Pivot_80
@_Get_Pivot_55:
      MOV     EAX,[T_2]
      JMP     @_Get_Pivot_80

@_Get_Pivot_60:
      CMP     BL,AL
      JNA     @_Get_Pivot_70
      MOV     EAX,[T_1]
      JMP     @_Get_Pivot_80
@_Get_Pivot_70:
      CMP     AL,CL
      JNA     @_Get_Pivot_75
      MOV     EAX,[T_2]
      JMP     @_Get_Pivot_80
@_Get_Pivot_75:
      MOV     EAX,[T_0]
      JMP     @_Get_Pivot_80

@_Get_Pivot_80:
      RET

@_Get_Pivot_90:
    End; { Asm }
  {$ELSE}
  Function Median_Of_Three (A : Integer; B : Integer; C : Integer) : Integer; Register;
    Var
      Key_A : Byte;
      Key_B : Byte;
      Key_C : Byte;

    Begin { Median_Of_Three }
      Key_A := M_P_Input_Buffer^ [M_P_A_I^ [A] + Depth];
      Key_B := M_P_Input_Buffer^ [M_P_A_I^ [B] + Depth];
      Key_C := M_P_Input_Buffer^ [M_P_A_I^ [C] + Depth];
      If Key_B > Key_C then
        Begin { then }
          If Key_A > Key_B then
            Begin { then }
              Result := B;
            End { then }
              else
            Begin { else }
              If Key_A > Key_C then
                Begin { then }
                  Result := A;
                End { then }
                  else
                Begin { else }
                  Result := C;
                End; { else }
            End; { else }
        End { then }
          else
        Begin { else }
          If Key_B > Key_A then
            Begin { then }
              Result := B;
            End { then }
              else
            Begin { else }
              If Key_A > Key_C then
                Begin { then }
                  Result := C;
                End { then }
                  else
                Begin { else }
                  Result := A;
                End; { else }
            End; { else }
        End; { else }
    End; { Median_Of_Three }

  Var
    M : Integer;
    D : Integer;

  Begin { T_ABC_BWT.Get_Pivot }
    M := (L + R) shr 1;
    If R - L >= WINARX_BWT_MEDIAN_THRESHOLD then
      Begin { then }
        D := (R - L) shr 3;
        L := Median_Of_Three (L, L + D, L + D + D);
        M := Median_Of_Three (M - D, M, M + D);
        R := Median_Of_Three (R - D - D, R - D, R);
      End; { then }
    M := Median_Of_Three (L, M, R);
    Result := M_P_Input_Buffer^ [M_P_A_I^ [M] + Depth];
  {$ENDIF}
  End; { T_ABC_BWT.Get_Pivot }


{-------------------------------------------------------------------------------------}
Procedure T_ABC_BWT.Ternary_Quicksort (L : Integer; R : Integer; Depth : Integer); Register;
{ Sorting an array from L until R with depth Depth                                    }
{-------------------------------------------------------------------------------------}
  Var
    A               : Integer;
    B               : Integer;
    C               : Integer;
    D               : Integer;
    Median          : Byte;
    I_0             : Integer;
    I_1             : Integer;
    Stack_Pointer   : Integer;
    A_L_Stack       : Array [0 .. WINARX_BWT_QUICKSORT_STACK_SIZE - 1] of Integer;
    A_R_Stack       : Array [0 .. WINARX_BWT_QUICKSORT_STACK_SIZE - 1] of Integer;
    A_Depth_Stack   : Array [0 .. WINARX_BWT_QUICKSORT_STACK_SIZE - 1] of Integer;
    {$IFDEF OPTIMIZATION_ON}
    P_A_I           : Pointer;
    P_Input_Buffer  : Pointer;
    {$ELSE}
    M               : Integer;
    I               : Integer;
    Temp            : Integer;
    {$ENDIF}


  Begin { T_ABC_BWT.Ternary_Quicksort }
    { Initialize stack pointer and stack }
    Stack_Pointer                  := 0;
    A_L_Stack [Stack_Pointer]      := L;
    A_R_Stack [Stack_Pointer]      := R;
    A_Depth_Stack [Stack_Pointer]  := Depth;
    Inc (Stack_Pointer);

    While Stack_Pointer > 0 do
      Begin { While }
        { POP data }
        Dec (Stack_Pointer);
        L     := A_L_Stack [Stack_Pointer];
        R     := A_R_Stack [Stack_Pointer];
        Depth := A_Depth_Stack [Stack_Pointer];

        { Get pivot }
        Median := Get_Pivot (L, R, Depth);

        A := L;
        B := L;
        C := R;
        D := R;

        {$IFDEF OPTIMIZATION_ON}
        P_A_I           := M_P_A_I;
        P_Input_Buffer  := M_P_Input_Buffer;

        { Run from left to right until greater element is reached, move all equal elements behind A }
        { Sub P11 }
        Asm
          PUSH    EBX
          PUSH    ESI
          PUSH    EDI

          MOV     ESI,[P_A_I]
          MOV     EDI,[P_Input_Buffer]

@_Ternary_Quicksort_100:
          MOV     EBX,[B]
          MOV     ECX,[C]
          CMP     EBX,ECX
          JA      @_Ternary_Quicksort_110
          MOV     EAX,[ESI + EBX*4]
          ADD     EAX,[Depth]
          MOV     AL,[EDI + EAX]
          CMP     AL,[Median]
          JA      @_Ternary_Quicksort_110
          JNZ     @_Ternary_Quicksort_105
          MOV     EAX,[A]
          CMP     EAX,EBX
          JZ      @_Ternary_Quicksort_104
          MOV     ECX,[ESI+EAX*4]
          MOV     EDX,[ESI+EBX*4]
          MOV     [ESI+EAX*4],EDX
          MOV     [ESI+EBX*4],ECX

@_Ternary_Quicksort_104:
          INC     [A]

@_Ternary_Quicksort_105:
          INC     [B]
          JMP     @_Ternary_Quicksort_100

@_Ternary_Quicksort_110:

@_Ternary_Quicksort_200:
          MOV     EBX,[B]
          MOV     ECX,[C]
          CMP     EBX,ECX
          JA      @_Ternary_Quicksort_210
          MOV     EAX,[ESI + ECX*4]
          ADD     EAX,[Depth]
          MOV     AL,[EDI + EAX]
          CMP     AL,[Median]
          JC      @_Ternary_Quicksort_210
          JNZ     @_Ternary_Quicksort_205
          MOV     EAX,ECX
          MOV     EBX,[D]
          MOV     ECX,[ESI+EAX*4]
          MOV     EDX,[ESI+EBX*4]
          MOV     [ESI+EAX*4],EDX
          MOV     [ESI+EBX*4],ECX

@_Ternary_Quicksort_204:
          DEC     [D]

@_Ternary_Quicksort_205:
          DEC     [C]
          JMP     @_Ternary_Quicksort_200

@_Ternary_Quicksort_210:


@_Ternary_Quicksort_300:
          MOV     EAX,[B]
          MOV     EBX,[C]
          CMP     EAX,EBX
          JA      @_Ternary_Quicksort_310
          MOV     ECX,[ESI+EAX*4]
          MOV     EDX,[ESI+EBX*4]
          MOV     [ESI+EAX*4],EDX
          MOV     [ESI+EBX*4],ECX

@_Ternary_Quicksort_304:
          INC     [B]

@_Ternary_Quicksort_305:
          DEC     [C]
          JMP     @_Ternary_Quicksort_100

@_Ternary_Quicksort_310:



@_Ternary_Quicksort_400:
          MOV     EAX,[A]
          SUB     EAX,[L]
          MOV     EBX,[B]
          SUB     EBX,[A]
          CMP     EAX,EBX
          JA      @_Ternary_Quicksort_405
          MOV     ECX,EAX
          JMP     @_Ternary_Quicksort_406

@_Ternary_Quicksort_405:
          MOV     ECX,EBX

@_Ternary_Quicksort_406:
          JECXZ   @_Ternary_Quicksort_411
          MOV     EAX,[L]
          MOV     EBX,[B]
          SUB     EBX,ECX
          LEA     EDI,[ESI+EBX*4]
          LEA     ESI,[ESI+EAX*4]

@_Ternary_Quicksort_410:
          MOV     EAX,[ESI]
          MOV     EBX,[EDI]
          MOV     [EDI],EAX
          ADD     EDI,4
          MOV     [ESI],EBX
          ADD     ESI,4
          DEC     ECX
          JNZ     @_Ternary_Quicksort_410

@_Ternary_Quicksort_411:

@_Ternary_Quicksort_500:
          MOV     EAX,[D]
          SUB     EAX,[C]
          MOV     EBX,[R]
          SUB     EBX,[D]
          CMP     EAX,EBX
          JA      @_Ternary_Quicksort_505
          MOV     ECX,EAX
          JMP     @_Ternary_Quicksort_506

@_Ternary_Quicksort_505:
          MOV     ECX,EBX

@_Ternary_Quicksort_506:
          JECXZ   @_Ternary_Quicksort_511
          MOV     EAX,[B]
          MOV     EBX,[R]
          SUB     EBX,ECX
          INC     EBX
          MOV     ESI,[P_A_I]
          LEA     EDI,[ESI+EBX*4]
          LEA     ESI,[ESI+EAX*4]

@_Ternary_Quicksort_510:
          MOV     EAX,[ESI]
          MOV     EBX,[EDI]
          MOV     [EDI],EAX
          ADD     EDI,4
          MOV     [ESI],EBX
          ADD     ESI,4
          DEC     ECX
          JNZ     @_Ternary_Quicksort_510

@_Ternary_Quicksort_511:


          POP     EDI
          POP     ESI
          POP     EBX
        End; { Asm }
        {$ELSE}
        { Sub P1 }
        While TRUE do
          Begin { While }
            { Run from left to right until greater element is reached, move all equal elements behind A }
            { Sub P11 }
            Temp := M_P_Input_Buffer^ [M_P_A_I^ [B] + Depth];
            While (B <= C) and (Temp <= Median) do
              Begin { While }
                If Temp = Median then
                  Begin { then }
                    If A <> B then
                      Begin { then }
                        Temp          := M_P_A_I^ [A];
                        M_P_A_I^ [A]  := M_P_A_I^ [B];
                        M_P_A_I^ [B]  := Temp;
                      End; { then }
                    Inc (A);
                  End; { then }
                Inc (B);
                Temp := M_P_Input_Buffer^ [M_P_A_I^ [B] + Depth];
              End; { While }

            { Run from right to left until smaller element is reached, move all equal elements behind D }
            { Sub P12 }
            Temp := M_P_Input_Buffer^ [M_P_A_I^ [C] + Depth];
            While (B <= C) and (Temp >= Median) do
              Begin { While }
                If Temp = Median then
                  Begin { then }
                    Temp          := M_P_A_I^ [C];
                    M_P_A_I^ [C]  := M_P_A_I^ [D];
                    M_P_A_I^ [D]  := Temp;
                    Dec (D);
                  End; { then }
                Dec (C);
                Temp := M_P_Input_Buffer^ [M_P_A_I^ [C] + Depth];
              End; { While }

            If B > C then
              Begin { then }
                Break;
              End { then }
                else
              Begin { else }
                { Sub P13 }
                Temp          := M_P_A_I^ [B];
                M_P_A_I^ [B]  := M_P_A_I^ [C];
                M_P_A_I^ [C]  := Temp;
                Inc (B);
                Dec (C);
              End; { else }
          End; { While }

        { Equal elements from left to middle }
        { Sub P2 }
        If A - L <= B - A then
          Begin { then }
            M := A - L;
          End { then }
            else
          Begin { else }
            M := B - A;
          End; { else }
        I_0 := L;
        I_1 := B - M;
        For I := 0 to M - 1 do
          Begin { For }
            Temp          := M_P_A_I [I_0];
            M_P_A_I [I_0] := M_P_A_I [I_1];
            M_P_A_I [I_1] := Temp;
            Inc (I_0);
            Inc (I_1);
          End; { For }

        { Equal elements from right to middle }
        { Sub P3 }
        If D - C <= R - D then
          Begin { then }
            M := D - C;
          End { then }
            else
          Begin { else }
            M := R - D;
          End; { else }
        I_0 := B;
        I_1 := R - M + 1;
        For I := 0 to M - 1 do
          Begin { For }
            Temp          := M_P_A_I [I_0];
            M_P_A_I [I_0] := M_P_A_I [I_1];
            M_P_A_I [I_1] := Temp;
            Inc (I_0);
            Inc (I_1);
          End; { For }
        {$ENDIF}

        I_0 := B - A;
        I_1 := D - C;

        { Sort smaller elements first }
        If I_0 > WINARX_BWT_BLOCK_SORTED then
          Begin { then }
            If I_0 <= WINARX_BWT_SHELL_RANGE_THRESHOLD then
              Begin { then }
                Shell_Sort (L, L + I_0 - 1, Depth);
              End { then }
                else
              Begin { else }
                A_L_Stack [Stack_Pointer]      := L;
                A_R_Stack [Stack_Pointer]      := L + I_0 - 1;
                A_Depth_Stack [Stack_Pointer]  := Depth;
                Inc (Stack_Pointer);
              End; { else }
          End; { then }

        { Sort medium elements }
        If ((R - I_1) - (L + I_0) + 1 <= WINARX_BWT_SHELL_RANGE_THRESHOLD) or (Depth > WINARX_BWT_SHELL_SORT_DEPTH_THRESHOLD) then
          Begin { then }
            If (R - I_1) - (L + I_0) + 1 > WINARX_BWT_BLOCK_SORTED then
              Begin { then }
                Shell_Sort (L + I_0, R - I_1, Depth + 1);
              End; { then }
          End { then }
            else
          Begin { else }
            A_L_Stack [Stack_Pointer]      := L + I_0;
            A_R_Stack [Stack_Pointer]      := R - I_1;
            A_Depth_Stack [Stack_Pointer]  := Depth + 1;
            Inc (Stack_Pointer);
          End; { else }

        { Sort greater elements last }
        If I_1 > WINARX_BWT_BLOCK_SORTED then
          Begin { then }
            If I_1 <= WINARX_BWT_SHELL_RANGE_THRESHOLD then
              Begin { then }
                Shell_Sort (R - I_1 + 1, R, Depth);
              End { then }
                else
              Begin { else }
                A_L_Stack [Stack_Pointer]      := R - I_1 + 1;
                A_R_Stack [Stack_Pointer]      := R;
                A_Depth_Stack [Stack_Pointer]  := Depth;
                Inc (Stack_Pointer);
              End; { else }
          End; { then }
      End; { While }
  End; { T_ABC_BWT.Ternary_Quicksort }


{-------------------------------------------------------------------------------------}
Procedure T_ABC_BWT.Suffix_Sort (Var Compress_Information : T_R_ABC_Compress_Information);
{ Sorting all suffices                                                                }
{-------------------------------------------------------------------------------------}
  Var
    I                           : Integer;
    Symbol                      : Byte;
    P_A_I_Index                 : Integer;
    Sorted_A_B_Left             : Integer;
    Sorted_D_C_Right            : Integer;
    Unsorted_D_Left             : Integer;
    Unsorted_A_Right            : Integer;
    Index                       : Integer;
    L                           : Integer;
    R                           : Integer;
    Unsorted_A_Sum_L            : Integer;
    Unsorted_A_Sum_H            : Integer;
    Unsorted_D_Sum_L            : Integer;
    Unsorted_D_Sum_H            : Integer;
    Direction_A_D               : Boolean;
    A_Bucket_Size_A             : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_Pointer_A          : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_Size_B             : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_Pointer_B          : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_Pointer_C          : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_Size_D             : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_Pointer_D          : Array [0 .. ABC_BYTE_MAXIMUM_SYMBOL] of Integer;
    A_Bucket_16_Pointer         : Array [0 .. ABC_WORD_MAXIMUM_SYMBOL + 1] of Integer;
    A_Bucket_16_Pointer_Copy    : Array [0 .. ABC_WORD_MAXIMUM_SYMBOL + 1] of Integer;
    P_A_Bucket_16_Pointer       : Pointer;
    P_A_Bucket_16_Pointer_Copy  : Pointer;
    P_A_Input                   : Pointer;
    P_A_I                       : Pointer;
    Block_Size                  : Integer;
    {$IFDEF OPTIMIZATION_ON}
    {$ELSE}
    Word_Symbol                 : Word;
    Last_Value                  : Integer;
    Unsorted_A_Sum              : Int64;
    Unsorted_D_Sum              : Int64;
    Index_0                     : Integer;
    Index_1                     : Integer;
    Value_0                     : Integer;
    Value_1                     : Integer;
    Value_Dif                   : Integer;
    Post_Symbol                 : Byte;
    {$ENDIF}

  Begin { T_ABC_BWT.Suffix_Sort }
    { Add EOF }
    For I := 0 to WINARX_BWT_EOF_BUFFER_SIZE * 4 do
      Begin { For }
        M_P_Input_Buffer^ [M_Block_Size + I] := WINARX_BWT_EOF;
      End; { For }
    Inc (M_Block_Size);

    {$IFDEF OPTIMIZATION_ON}
    { Initialize variables }
    P_A_Bucket_16_Pointer       := @A_Bucket_16_Pointer [0];
    P_A_Bucket_16_Pointer_Copy  := @A_Bucket_16_Pointer_Copy [0];
    P_A_I                       := M_P_A_I;
    P_A_Input                   := M_P_Input_Buffer;
    Block_Size                  := M_Block_Size;
    Asm
      { PUSH register }
      PUSH    EDI
      PUSH    ESI
      PUSH    EBX


      { Clear array }
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,ABC_WORD_MAXIMUM_SYMBOL + 2
      XOR     EAX,EAX
@_Suffix_Sort_0:
      MOV     [EDI],EAX
      ADD     EDI,4
      DEC     ECX
      JNZ     @_Suffix_Sort_0


      { Calculate frequencies of 16-bit buckets inclusive EOF }
      MOV     ESI,[P_A_Input]
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,[Block_Size]
      XOR     EAX,EAX
      MOV     AH,[ESI]
      INC     ESI
      JECXZ   @_Suffix_Sort_11

@_Suffix_Sort_10:
      MOV     AL,[ESI]
      INC     ESI
      Inc     DWORD PTR [EDI + EAX*4]   { increment bucket frequency }
      MOV     AH,AL                     {  process next symbol }
      DEC     ECX
      JNZ     @_Suffix_Sort_10


      { Set start of 16-bit D buckets so, that each bucket pointer points to the first field in his bucket }
@_Suffix_Sort_11:
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,ABC_WORD_MAXIMUM_SYMBOL + 2
      XOR     EAX,EAX
      XOR     EDX,EDX

@_Suffix_Sort_20:
      MOV     EBX,[EDI]
      ADD     EAX,EDX
      STOSD
      MOV     EDX,EBX
      DEC     ECX
      JNZ     @_Suffix_Sort_20


      { Save array }
      MOV     ESI,[P_A_Bucket_16_Pointer]
      MOV     EDI,[P_A_Bucket_16_Pointer_Copy]
      MOV     ECX,ABC_WORD_MAXIMUM_SYMBOL + 1
@_Suffix_Sort_200:
      MOV     EAX,[ESI]
      ADD     ESI,4
      MOV     [EDI],EAX
      ADD     EDI,4
      DEC     ECX
      JNZ     @_Suffix_Sort_200


      { Calculate sum of squares for A buckets }
      XOR     ECX,ABC_BYTE_MAXIMUM_SYMBOL + 1
      XOR     EBX,EBX
      XOR     EDI,EDI
      XOR     ESI,ESI

@_Suffix_Sort_30:
      PUSH    ECX
      MOV     ECX,EBX
      MOV     CL,CH
      PUSH    ESI
      MOV     ESI,[P_A_Bucket_16_Pointer]
      MOV     EAX,[ESI + ECX*4]
      SUB     EAX,[ESI + EBX*4]
      POP     ESI
      CMP     EAX,WINARX_BWT_BLOCK_SORTED
      JLE     @_Suffix_Sort_31
      IMUL    EAX
      ADD     ESI,EAX
      ADD     EDI,EDX
@_Suffix_Sort_31:
      INC     BH
      POP     ECX
      DEC     ECX
      JNZ     @_Suffix_Sort_30
      MOV     [Unsorted_A_Sum_L],ESI
      MOV     [Unsorted_A_Sum_H],EDI


      { Calculate sum of squares for D buckets }
      XOR     ECX,ABC_BYTE_MAXIMUM_SYMBOL + 1
      XOR     EBX,EBX
      XOR     EDI,EDI
      XOR     ESI,ESI

@_Suffix_Sort_40:
      PUSH    ECX
      MOV     ECX,EBX
      MOV     CL,CH
      PUSH    ESI
      MOV     ESI,[P_A_Bucket_16_Pointer]
      INC     BH
      MOV     EAX,[ESI + EBX*4]
      DEC     BH
      SUB     EAX,[ESI + ECX*4 + 4]
      POP     ESI
      CMP     EAX,WINARX_BWT_BLOCK_SORTED
      JLE     @_Suffix_Sort_41
      IMUL    EAX
      ADD     ESI,EAX
      ADD     EDI,EDX
@_Suffix_Sort_41:
      INC     BH
      POP     ECX
      DEC     ECX
      JNZ     @_Suffix_Sort_40
      MOV     [Unsorted_D_Sum_L],ESI
      MOV     [Unsorted_D_Sum_H],EDI


      { Set direction }
      MOV     [Direction_A_D],FALSE
      MOV     EAX,[Unsorted_A_Sum_H]
      SUB     EAX,[Unsorted_D_Sum_H]
      JZ      @_Suffix_Sort_50
      JC      @_Suffix_Sort_51
@_Suffix_Sort_50:
      MOV     EAX,[Unsorted_A_Sum_L]
      SUB     EAX,[Unsorted_D_Sum_L]
      JC      @_Suffix_Sort_51
      MOV     [Direction_A_D],TRUE

@_Suffix_Sort_51:
      CMP     [Direction_A_D],FALSE
      JZ      @_Suffix_Sort_70


      { Set start of 16-bit D buckets so, that each bucket pointer points to the last field in his bucket }
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,ABC_WORD_MAXIMUM_SYMBOL + 1
      LEA     EDI,[EDI + ECX*4]
      MOV     EAX,[EDI]
      SUB     EDI,4

@_Suffix_Sort_60:
      DEC     EAX
      MOV     EBX,[EDI]
      MOV     [EDI],EAX
      MOV     EAX,EBX
      SUB     EDI,4
      DEC     ECX
      JNZ     @_Suffix_Sort_60


      { Sort all symbols of D buckets into index array }
      MOV     ESI,[P_A_Input]
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,[Block_Size]
      ADD     ESI,ECX
      DEC     ECX                       { Index starts at 0, therefore decrement ECX }
      MOV     EDX,[P_A_I]
      XOR     EAX,EAX
      MOV     AL,[ESI]
      DEC     ESI

@_Suffix_Sort_61:
      MOV     AH,[ESI]                  { Get first byte }
      DEC     ESI

      CMP     AH,AL                     { Is it a D bucket ??? }
      JNC     @_Suffix_Sort_62          { NO --> }

      MOV     EBX,[EDI + EAX*4]         { YES }
      XCHG    EDI,EDX
      MOV     [EDI + EBX*4],ECX
      XCHG    EDI,EDX
      DEC     DWORD PTR [EDI + EAX*4]

@_Suffix_Sort_62:
      MOV     AL,AH                     { Process next symbol }
      DEC     ECX
      JNS     @_Suffix_Sort_61
      JMP     @_Suffix_Sort_80


@_Suffix_Sort_70:
      { Sort all symbols of A buckets into index array }
      MOV     ESI,[P_A_Input]
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,[Block_Size]
      LEA     ESI,[ESI + ECX]
      DEC     ECX                       { Index starts at 0, therefore decrement ECX }
      MOV     EDX,[P_A_I]
      XOR     EAX,EAX
      MOV     AL,[ESI]
      DEC     ESI

@_Suffix_Sort_71:
      MOV     AH,[ESI]                  { Get first byte }
      DEC     ESI

      CMP     AL,AH                     { Is it a A bucket ??? }
      JNC     @_Suffix_Sort_72          { NO --> }

      MOV     EBX,[EDI + EAX*4]         { YES }
      XCHG    EDI,EDX
      MOV     [EDI + EBX*4],ECX
      XCHG    EDI,EDX
      INC     DWORD PTR [EDI + EAX*4]

@_Suffix_Sort_72:
      MOV     AL,AH                     { Process next symbol }
      DEC     ECX
      JNS     @_Suffix_Sort_71


@_Suffix_Sort_80:
      { Reset array }
      MOV     ESI,[P_A_Bucket_16_Pointer_Copy]
      MOV     EDI,[P_A_Bucket_16_Pointer]
      MOV     ECX,ABC_WORD_MAXIMUM_SYMBOL + 1
@_Suffix_Sort_800:
      MOV     EAX,[ESI]
      ADD     ESI,4
      MOV     [EDI],EAX
      ADD     EDI,4
      DEC     ECX
      JNZ     @_Suffix_Sort_800


      { POP register }
      POP     EBX
      POP     ESI
      POP     EDI
    End; { Asm }
    {$ELSE}
    { Clear array }
    FillChar (A_Bucket_16_Pointer [0], (ABC_WORD_MAXIMUM_SYMBOL + 2) * SizeOf (Integer), $00);

    { Calculate frequencies of 16-bit buckets inclusive EOF }
    For I := 0 to M_Block_Size - 1 do
      Begin { For }
        Word_Symbol := (M_P_Input_Buffer^ [I] shl ABC_BYTE_BIT_SIZE) or (M_P_Input_Buffer^ [I + 1]);
        Inc (A_Bucket_16_Pointer [Word_Symbol]);
      End; { For }

    { Set start of 16-bit D buckets so, that each bucket pointer points to the first field in his bucket }
    Index := 0;
    For I := 0 to ABC_WORD_MAXIMUM_SYMBOL + 2 - 1 do
      Begin { For }
        Last_Value              := A_Bucket_16_Pointer [I];
        A_Bucket_16_Pointer [I] := Index;
        Inc (Index, Last_Value);
      End; { For }

    { Save array }
    Move (A_Bucket_16_Pointer, A_Bucket_16_Pointer_Copy, (ABC_WORD_MAXIMUM_SYMBOL + 1) * SizeOf (Integer));

    { Calculate sum of squares for A buckets }
    Unsorted_A_Sum := 0;
    For I := 0 to ABC_BYTE_MAXIMUM_SYMBOL + 1 - 1 do
      Begin { For }
        Index_0   := I shl ABC_BYTE_BIT_SIZE;
        Index_1   := I or Index_0;
        Value_0   := A_Bucket_16_Pointer [Index_0];
        Value_1   := A_Bucket_16_Pointer [Index_1];
        Value_Dif := Value_1 - Value_0;
        If Value_Dif > WINARX_BWT_BLOCK_SORTED then
          Begin { then }
            Unsorted_A_Sum := Unsorted_A_Sum + (Int64 (Value_Dif) * Int64 (Value_Dif));
          End; { then }
      End; { For }

    { Calculate sum of squares for D buckets }
    Unsorted_D_Sum := 0;
    For I := 0 to ABC_BYTE_MAXIMUM_SYMBOL + 1 - 1 do
      Begin { For }
        Index_0   := (I + 1) shl ABC_BYTE_BIT_SIZE;
        Index_1   := I or (I shl ABC_BYTE_BIT_SIZE) + 1;
        Value_0   := A_Bucket_16_Pointer [Index_0];
        Value_1   := A_Bucket_16_Pointer [Index_1];
        Value_Dif := Value_0 - Value_1;
        If Value_Dif > WINARX_BWT_BLOCK_SORTED then
          Begin { then }
            Unsorted_D_Sum := Unsorted_D_Sum + (Int64 (Value_Dif) * Int64 (Value_Dif));
          End; { then }
      End; { For }

    { Set direction }
    If Unsorted_A_Sum >= Unsorted_D_Sum then
      Begin { then }
        Direction_A_D := TRUE;

        { Set start of 16-bit D buckets so, that each bucket pointer points to the last field in his bucket }
        For I := 0 to ABC_WORD_MAXIMUM_SYMBOL + 1 - 1 do
          Begin { For }
            A_Bucket_16_Pointer [I] := A_Bucket_16_Pointer [I + 1] - 1;
          End; { For }

        { Sort all symbols of D buckets into index array }
        For I := M_Block_Size - 1 downto 0 do
          Begin { For }
            Symbol      := M_P_Input_Buffer^ [I];
            Post_Symbol := M_P_Input_Buffer^ [I + 1];
            If Symbol < Post_Symbol then
              Begin { then }
                Index             := A_Bucket_16_Pointer [(Symbol shl ABC_BYTE_BIT_SIZE) or Post_Symbol];
                M_P_A_I^ [Index]  := I;
                Dec (A_Bucket_16_Pointer [(Symbol shl ABC_BYTE_BIT_SIZE) or Post_Symbol]);
              End; { then }
          End; { For }
      End { then }
        else
      Begin { else }
        Direction_A_D := FALSE;

        { Sort all symbols of A buckets into index array }
        For I := M_Block_Size - 1 downto 0 do
          Begin { For }
            Symbol      := M_P_Input_Buffer^ [I];
            Post_Symbol := M_P_Input_Buffer^ [I + 1];
            If Post_Symbol < Symbol then
              Begin { then }
                Index             := A_Bucket_16_Pointer [(Symbol shl ABC_BYTE_BIT_SIZE) or Post_Symbol];
                M_P_A_I^ [Index]  := I;
                Inc (A_Bucket_16_Pointer [(Symbol shl ABC_BYTE_BIT_SIZE) or Post_Symbol]);
              End; { then }
          End; { For }
      End; { else }

    { Reset array }
    Move (A_Bucket_16_Pointer_Copy, A_Bucket_16_Pointer, (ABC_WORD_MAXIMUM_SYMBOL + 1) * SizeOf (Integer));
    {$ENDIF}

    { Set size and pointer array }
    A_Bucket_Size_A [0]     := A_Bucket_16_Pointer [(0 shl ABC_BYTE_BIT_SIZE) + 0] - A_Bucket_16_Pointer [(0 shl ABC_BYTE_BIT_SIZE)];
    A_Bucket_Size_B [0]     := A_Bucket_16_Pointer [(0 shl ABC_BYTE_BIT_SIZE) + 0 + 1] - A_Bucket_16_Pointer [(0 shl ABC_BYTE_BIT_SIZE) + 0];
    A_Bucket_Size_D [0]     := A_Bucket_16_Pointer [((0 + 1) shl ABC_BYTE_BIT_SIZE)] - A_Bucket_16_Pointer [(0 shl ABC_BYTE_BIT_SIZE) + 0 + 1];
    A_Bucket_Pointer_A [0]  := 0;
    A_Bucket_Pointer_B [0]  := A_Bucket_Pointer_A [0] + A_Bucket_Size_A [0];
    A_Bucket_Pointer_C [0]  := A_Bucket_Pointer_B [0] + A_Bucket_Size_B [0] - 1;
    A_Bucket_Pointer_D [0]  := A_Bucket_Pointer_C [0] + A_Bucket_Size_D [0];
    For I := 1 to ABC_BYTE_MAXIMUM_SYMBOL do
      Begin { For }
        A_Bucket_Size_A [I]     := A_Bucket_16_Pointer [(I shl ABC_BYTE_BIT_SIZE) + I] - A_Bucket_16_Pointer [(I shl ABC_BYTE_BIT_SIZE)];
        A_Bucket_Size_B [I]     := A_Bucket_16_Pointer [(I shl ABC_BYTE_BIT_SIZE) + I + 1] - A_Bucket_16_Pointer [(I shl ABC_BYTE_BIT_SIZE) + I];
        A_Bucket_Size_D [I]     := A_Bucket_16_Pointer [((I + 1) shl ABC_BYTE_BIT_SIZE)] - A_Bucket_16_Pointer [(I shl ABC_BYTE_BIT_SIZE) + I + 1];
        A_Bucket_Pointer_A [I]  := A_Bucket_Pointer_D [I - 1] + 1;
        A_Bucket_Pointer_B [I]  := A_Bucket_Pointer_A [I] + A_Bucket_Size_A [I];
        A_Bucket_Pointer_C [I]  := A_Bucket_Pointer_B [I] + A_Bucket_Size_B [I] - 1;
        A_Bucket_Pointer_D [I]  := A_Bucket_Pointer_C [I] + A_Bucket_Size_D [I];
      End; { For }

    { Initialize pointer }
    Sorted_A_B_Left   := 0;
    Sorted_D_C_Right  := M_Block_Size - 1;
    Unsorted_D_Left   := 0;
    Unsorted_A_Right  := ABC_BYTE_MAXIMUM_SYMBOL;

    { Set index array for EOF }
    I                         := M_Block_Size - 1;
    Symbol                    := M_P_Input_Buffer^ [I];
    P_A_I_Index               := A_Bucket_Pointer_B [Symbol];
    M_P_A_I^ [P_A_I_Index]    := I;
    Inc (A_Bucket_Pointer_B [Symbol]);

    If Direction_A_D = TRUE then
      Begin { then }
        { Initialize field for first input symbol }
        I                     := -1;
        M_P_Input_Buffer^ [I] := 0;

        { Handle all buckets from left to right }
        While Unsorted_D_Left <= ABC_BYTE_MAXIMUM_SYMBOL do
          Begin { While }
            { Sort all not sorted D buckets from left to right }
            If A_Bucket_Size_D [Unsorted_D_Left] > WINARX_BWT_BLOCK_SORTED then
              Begin { then }
                For I := Unsorted_D_Left + 1 to ABC_BYTE_MAXIMUM_SYMBOL do
                  Begin { For }
                    L := A_Bucket_16_Pointer [(Unsorted_D_Left shl ABC_BYTE_BIT_SIZE) + I];
                    R := A_Bucket_16_Pointer [(Unsorted_D_Left shl ABC_BYTE_BIT_SIZE) + I + 1] - 1;
                    If R - L + 1 > WINARX_BWT_BLOCK_SORTED then
                      Begin { then }
                        If R - L + 1 <= WINARX_BWT_SHELL_RANGE_THRESHOLD then
                          Begin { then }
                            Shell_Sort (L, R, 1);
                          End { then }
                            else
                          Begin { else }
                            Ternary_Quicksort (L, R, 1);
                          End; { else }
                      End; { else }
                  End; { For }
              End; { then }

            { Fill all B and D buckets }
            If A_Bucket_Size_B [Unsorted_D_Left] > 0 then
              Begin { then }
                { Fill data of C bucket from D and C bucket }
                I := A_Bucket_Pointer_D [Unsorted_D_Left];
                While I > A_Bucket_Pointer_C [Unsorted_D_Left] do
                  Begin { While }
                    Index := M_P_A_I^ [I];
                    Assert (Index = 0, 'Index = 0; -> ; C-Korb; ');
                    If (Index > 0) and (M_P_Input_Buffer^ [Index - 1] = M_P_Input_Buffer^ [Index]) then
                      Begin { then }
                        M_P_A_I^ [A_Bucket_Pointer_C [Unsorted_D_Left]] := Index - 1;
                        Dec (A_Bucket_Pointer_C [Unsorted_D_Left]);
                      End; { then }
                    Dec (I);
                  End; { While }

                { Fill data of B bucket from A and B bucket }
                I := A_Bucket_Pointer_D [Unsorted_D_Left] - A_Bucket_Size_D [Unsorted_D_Left] - A_Bucket_Size_B [Unsorted_D_Left] - A_Bucket_Size_A [Unsorted_D_Left] + 1;
                While I < A_Bucket_Pointer_B [Unsorted_D_Left] do
                  Begin { While }
                    Index := M_P_A_I^ [I];
                    Assert (Index = 0, 'Index = 0; -> ; B-Korb; ');
                    If (Index > 0) and (M_P_Input_Buffer^ [Index - 1] = M_P_Input_Buffer^ [Index]) then
                      Begin { then }
                        M_P_A_I^ [A_Bucket_Pointer_B [Unsorted_D_Left]] := Index - 1;
                        Inc (A_Bucket_Pointer_B [Unsorted_D_Left]);
                      End; { then }
                    Inc (I);
                  End; { While }
              End; { then }

            { Fill all A buckets }
            While Sorted_A_B_Left <= A_Bucket_Pointer_D [Unsorted_D_Left] do
              Begin { While }
                Index   := M_P_A_I^ [Sorted_A_B_Left];
                Symbol  := M_P_Input_Buffer^ [Index - 1];

                { Output data }
                If Index > 0 then
                  Begin { then }
                    M_P_Output_Buffer [Sorted_A_B_Left] := Symbol;
                  End { then }
                    else
                  Begin { else }
                    M_EOF_Index := Sorted_A_B_Left;
                  End; { else }

                If Symbol > M_P_Input_Buffer^ [Index] then
                  Begin { then }
                    { Complete A bucket }
                    M_P_A_I^ [A_Bucket_Pointer_A [Symbol]] := Index - 1;
                    Inc (A_Bucket_Pointer_A [Symbol]);
                  End; { then }
                Inc (Sorted_A_B_Left);
              End; { While }

            { Next bucket }
            Inc (Unsorted_D_Left);
          End; { While }

        { Process last symbol }
        Index := M_P_A_I^ [Sorted_A_B_Left];
        If Index > 0 then
          Begin { then }
            M_P_Output_Buffer [Sorted_A_B_Left] := M_P_Input_Buffer^ [Index - 1];
          End; { then }
      End { then }
        else
      Begin { else }
        { Initialize field for first input symbol }
        I                     := -1;
        M_P_Input_Buffer^ [I] := ABC_BYTE_MAXIMUM_SYMBOL;

        { Handle all buckets from right to left }
        While Unsorted_A_Right >= 0 do
          Begin { While }
            { Sort all not sorted A buckets from right to left }
            If A_Bucket_Size_A [Unsorted_A_Right] > WINARX_BWT_BLOCK_SORTED then
              Begin { then }
                For I := 0 to Unsorted_A_Right - 1 do
                  Begin { For }
                    L := A_Bucket_16_Pointer [(Unsorted_A_Right shl ABC_BYTE_BIT_SIZE) + I];
                    R := A_Bucket_16_Pointer [(Unsorted_A_Right shl ABC_BYTE_BIT_SIZE) + I + 1] - 1;
                    If R - L + 1 > WINARX_BWT_BLOCK_SORTED then
                      Begin { then }
                        If R - L + 1 <= WINARX_BWT_SHELL_RANGE_THRESHOLD then
                          Begin { then }
                            Shell_Sort (L, R, 1);
                          End { then }
                            else
                          Begin { else }
                            Ternary_Quicksort (L, R, 1);
                          End; { else }
                      End; { else }
                  End; { For }
              End; { then }

            { Fill all B and C buckets }
            If A_Bucket_Size_B [Unsorted_A_Right] > 0 then
              Begin { then }
                { Fill data of B bucket from A and B bucket }
                I := A_Bucket_Pointer_A [Unsorted_A_Right];
                While I < A_Bucket_Pointer_B [Unsorted_A_Right] do
                  Begin { While }
                    Index := M_P_A_I^ [I];
                    Assert (Index = 0, 'Index = 0; <- ; B-Korb; ');
                    If (Index > 0) and (M_P_Input_Buffer^ [Index - 1] = M_P_Input_Buffer^ [Index]) then
                      Begin { then }
                        M_P_A_I^ [A_Bucket_Pointer_B [Unsorted_A_Right]] := Index - 1;
                        Inc (A_Bucket_Pointer_B [Unsorted_A_Right]);
                      End; { then }
                    Inc (I);
                  End; { While }

                { Fill data of C bucket from D and C bucket }
                I := A_Bucket_Pointer_A [Unsorted_A_Right] + A_Bucket_Size_A [Unsorted_A_Right] + A_Bucket_Size_B [Unsorted_A_Right] + A_Bucket_Size_D [Unsorted_A_Right] - 1;
                While I > A_Bucket_Pointer_C [Unsorted_A_Right] do
                  Begin { While }
                    Index := M_P_A_I^ [I];
                    Assert (Index = 0, 'Index = 0; <- ; C-Korb; ');
                    If (Index > 0) and (M_P_Input_Buffer^ [Index - 1] = M_P_Input_Buffer^ [Index]) then
                      Begin { then }
                        M_P_A_I^ [A_Bucket_Pointer_C [Unsorted_A_Right]] := Index - 1;
                        Dec (A_Bucket_Pointer_C [Unsorted_A_Right]);
                      End; { then }
                    Dec (I);
                  End; { While }
              End; { then }

            { Fill all D buckets }
            While Sorted_D_C_Right >= A_Bucket_Pointer_A [Unsorted_A_Right] do
              Begin { While }
                Index   := M_P_A_I^ [Sorted_D_C_Right];
                Symbol  := M_P_Input_Buffer^ [Index - 1];

                { Output data }
                If Index > 0 then
                  Begin { then }
                    M_P_Output_Buffer [Sorted_D_C_Right] := Symbol;
                  End { then }
                    else
                  Begin { else }
                    M_EOF_Index := Sorted_D_C_Right;
                  End; { else }

                If Symbol < M_P_Input_Buffer^ [Index] then
                  Begin { then }
                    { Complete D bucket }
                    M_P_A_I^ [A_Bucket_Pointer_D [Symbol]] := Index - 1;
                    Dec (A_Bucket_Pointer_D [Symbol]);
                  End; { then }
                Dec (Sorted_D_C_Right);
              End; { While }

            { Next bucket }
            Dec (Unsorted_A_Right);
          End; { While }
      End; { else }
  End; { T_ABC_BWT.Suffix_Sort }


{-------------------------------------------------------------------------------------}
Procedure T_ABC_BWT.Encode (Var Compress_Information : T_R_ABC_Compress_Information);
{ Sorting M_P_Input_Buffer as a suffix array and outputs column L and index of        }
{ EOF symbol                                                                          }
{-------------------------------------------------------------------------------------}
  Begin { T_ABC_BWT.Encode }
    If Compress_Information.P_Input_Record^.Size <= 0 then
      Begin { then }
        { Switch buffer }
        ABC_MCP.Switch_Block (Compress_Information);
        Exit;
      End; { then }

    { Set buffer }
    M_P_A_I := Compress_Information.P_Temp_A_0;
    Inc (Integer (Pointer (M_P_A_I)), SizeOf (Integer));

    { Initialize variables }
    M_P_Input_Buffer  := Compress_Information.P_Input_Record^.P_Buffer;
    M_P_Output_Buffer := Compress_Information.P_Output_Record^.P_Buffer;
    M_Block_Size      := Compress_Information.P_Input_Record^.Size;

    { Sort data }
    Suffix_Sort (Compress_Information);
    M_P_Output_Buffer^ [M_EOF_Index] := WINARX_BWT_EOF;

    { Reset buffer }
    Dec (Integer (Pointer (M_P_A_I)), SizeOf (Integer));

    { Save EOF index }
    Compress_Information.P_Administration_Block^.P_Buffer^ [Compress_Information.P_Administration_Block^.Cursor] := M_EOF_Index and $FF;
    Inc (Compress_Information.P_Administration_Block^.Cursor);
    Compress_Information.P_Administration_Block^.P_Buffer^ [Compress_Information.P_Administration_Block^.Cursor] := (M_EOF_Index shr 8) and $FF;
    Inc (Compress_Information.P_Administration_Block^.Cursor);
    Compress_Information.P_Administration_Block^.P_Buffer^ [Compress_Information.P_Administration_Block^.Cursor] := M_EOF_Index shr 16;
    Inc (Compress_Information.P_Administration_Block^.Cursor);

    { Set output size }
    Compress_Information.P_Output_Record^.Size := Compress_Information.P_Input_Record^.Size + 1;
  End; { T_ABC_BWT.Encode }


{-------------------------------------------------------------------------------------}
Procedure T_ABC_BWT.Decode (Var Compress_Information : T_R_ABC_Compress_Information);
{ Decoding data                                                                       }
{-------------------------------------------------------------------------------------}
  Var
    P_Input_Buffer    : T_P_ABC_Byte_Array;
    P_Output_Buffer   : T_P_ABC_Byte_Array;
    Block_Size        : Integer;
    P_A_F             : T_P_ABC_Integer_Array;
    P_A_T             : T_P_ABC_Integer_Array;
    {$IFDEF OPTIMIZATION_ON}
    EOF_Index         : Integer;
    {$ELSE}
    I                 : Integer;
    Symbol            : Byte;
    P                 : Integer;
    {$ENDIF}

  Begin { T_ABC_BWT.Decode }
    If Compress_Information.P_Input_Record^.Size <= 0 then
      Begin { then }
        { Puffer umdrehen }
        ABC_MCP.Switch_Block (Compress_Information);
        Exit;
      End; { then }

    { Read EOF index }
    Dec (Compress_Information.P_Administration_Block^.Cursor);
    M_EOF_Index := Compress_Information.P_Administration_Block^.P_Buffer^ [Compress_Information.P_Administration_Block^.Cursor] shl 16;
    Dec (Compress_Information.P_Administration_Block^.Cursor);
    M_EOF_Index := M_EOF_Index + (Compress_Information.P_Administration_Block^.P_Buffer^ [Compress_Information.P_Administration_Block^.Cursor] shl 8);
    Dec (Compress_Information.P_Administration_Block^.Cursor);
    M_EOF_Index := M_EOF_Index + Compress_Information.P_Administration_Block^.P_Buffer^ [Compress_Information.P_Administration_Block^.Cursor];

    { Save parameter }
    P_Input_Buffer  := Compress_Information.P_Input_Record^.P_Buffer;
    P_Output_Buffer := Compress_Information.P_Output_Record^.P_Buffer;
    Block_Size      := Compress_Information.P_Input_Record^.Size;
    P_A_F           := Compress_Information.P_Temp_A_0;
    P_A_T           := Compress_Information.P_Temp_A_1;

    { Set output size }
    Compress_Information.P_Output_Record^.Size := Compress_Information.P_Input_Record^.Size - 1;

    {$IFDEF OPTIMIZATION_ON}
    EOF_Index := M_EOF_Index;
    Asm
      PUSH    EBX
      PUSH    ESI
      PUSH    EDI

      MOV     ESI,[P_A_F]
      MOV     EDI,[P_A_T]
      MOV     EBX,[P_Input_Buffer]


      { Initialize P_A_F }
      MOV     ECX,ABC_BYTE_MAXIMUM_SYMBOL
      XOR     EDX,EDX
@_Decode_00:
      MOV     [ESI + ECX*4],EDX
      SUB     ECX,1
      JNC     @_Decode_00


      { Calculate symbol frequencies and set P_A_T }
      INC     DWORD PTR [ESI]
      XOR     ECX,ECX
      XOR     EAX,EAX

@_Decode_10:
      CMP     ECX,[EOF_Index]
      JZ      @_Decode_11
      MOV     AL,[EBX + ECX]
      INC     ECX
      MOV     EDX,[ESI + EAX*4]
      SHL     EDX,8
      MOV     DL,AL
      INC     DWORD PTR [ESI + EAX*4]
      MOV     [EDI + ECX*4 - 4],EDX

      CMP     ECX,[Block_Size]
      JC      @_Decode_10
      JMP     @_Decode_20

@_Decode_11:
      INC     ECX
      MOV     DWORD PTR [EDI + ECX*4],0
      CMP     ECX,[Block_Size]
      JNC     @_Decode_20

@_Decode_12:
      MOV     AL,[EBX + ECX]
      INC     ECX
      MOV     EDX,[ESI + EAX*4]
      SHL     EDX,8
      MOV     DL,AL
      INC     DWORD PTR [ESI + EAX*4]
      MOV     [EDI + ECX*4 - 4],EDX

      CMP     ECX,[Block_Size]
      JC      @_Decode_12


@_Decode_20:
      { Cumulate symbol frequencies }
      MOV     ECX,1
@_Decode_21:
      MOV     EAX,[ESI + ECX*4 - 4]
      ADD     [ESI + ECX*4],EAX

      INC     ECX
      CMP     ECX,ABC_BYTE_MAXIMUM_SYMBOL + 1
      JC      @_Decode_21


      { Move symbol frequencies one field ahead}
      MOV     ECX,ABC_BYTE_MAXIMUM_SYMBOL
@_Decode_30:
      MOV     EAX,[ESI + ECX*4 - 4]
      MOV     [ESI + ECX*4],EAX

      DEC     ECX
      JNZ     @_Decode_30
      MOV     DWORD PTR [ESI + ECX*4],0


      { Output symbols from bottom to top }
      MOV     ECX,[Block_Size]
      DEC     ECX
      PUSH    EBP
      MOV     EBP,[P_Output_Buffer]
      DEC     EBP
      XOR     EAX,EAX
      XOR     EDX,EDX
      JECXZ   @_Decode_41

@_Decode_40:
      MOV     EDX,[EDI + EDX*4]
      MOV     AL,DL
      MOV     [EBP + ECX],DL
      SHR     EDX,8
      ADD     EDX,[ESI + EAX*4]
      DEC     ECX
      JNZ     @_Decode_40
@_Decode_41:
      POP     EBP


      POP     EDI
      POP     ESI
      POP     EBX
    End; { Asm }
    {$ELSE}
    { Initialize P_A_F }
    FillChar (P_A_F^ [0], (ABC_BYTE_MAXIMUM_SYMBOL + 1) * SizeOf (Integer), $00);

    { Calcualte symbol frequencies and set P_A_T }
    Inc (P_A_F^ [WINARX_BWT_EOF]);
    For I := 0 to Block_Size - 1 do
      Begin { For }
        If I <> M_EOF_Index then
          Begin { then }
            Symbol      := P_Input_Buffer^ [I];
            P_A_T^ [I]  := (P_A_F^ [Symbol] shl 8) or Symbol;
            Inc (P_A_F^ [Symbol]);
          End { then }
            else
          Begin { else }
            P_A_T^ [I]  := 0;
          End; { else }
      End; { For }

    { Cumulate symbol frequencies }
    For I := 1 to ABC_BYTE_MAXIMUM_SYMBOL do
      Begin { For }
        P_A_F^ [I] := P_A_F^ [I] + P_A_F^ [I - 1];
      End; { For }

    { Move symbol frequencies one field ahead}
    For I := ABC_BYTE_MAXIMUM_SYMBOL downto 1 do
      Begin { For }
        P_A_F^ [I] := P_A_F^ [I - 1];
      End; { For }
    P_A_F^ [0] := 0;

    { Output symbols from bottom to top }
    P := 0;
    For I := Block_Size - 2 downto 0 do
      Begin { For }
        P                     := P_A_T^ [P];
        Symbol                := P and $FF;
        P_Output_Buffer^ [I]  := Symbol;
        P                     := (P shr 8) + P_A_F^ [Symbol];
      End; { For }
    {$ENDIF}
  End; { T_ABC_BWT.Decode }


End.

