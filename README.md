The README
==========

The project is evolving. The aim is to make a CLI terminal with good
features, and bring the console experience into the modern age. A new
terminal kind (VT303) is where I'm starting. It aims for the middle ground
between a simple teletype and a complex X Windows installation.

To keep application development simple it will operate a simple CLI with
a command language interpreter, have a base font of 512 characters and
support a low resolution but colourful graphics and character mapped display.

The complexity of using graphics toolkits for simple graphics has the
disadvantage of complex code to bootstrap what in essence is a simple prototype
graphics requirement. On the other hand a text console is limited in
doing text and some colour by ANSI.

This leads to the silly situation of deciding on a tools kit, and spending
time to use it while it consumes much resources. A headerless distribution
will not necessarily need the full complexity of X Windows or similar, 
and so a terminal standard with a nice level and fast learning curve of
graphics will be ideal for online tools.

You may ask yourself why a browser and some server backend stuff might not
do just as well? A 16 kB maximum complexity to the "page size" and no
need to set up a full ssh or https install and configure might be a good answer.

A new client server model can be developed with the benefit of hindsight.
If you're not too critical of alias perfect graphics and prefere content over
style, this could be what you're looking for.

A Rough Graphics Description
----------------------------

* An 80 `*` 48 character display (640 `*` 480) with standard font.
  * font 8 `*` 10 px (512 ch standard (128 fixed ASKII), 1280 ch max).
* A 256 `*` 120 pixel display scaled (640 `*` 480 (2.5 `*` 4)) and centred.
  * (X) 5 "character" pixels per 2 "graphics" pixels (5 ch = 16 px).
  * (Y) 4 "character" pixels per 1 "graphics" pixel (2 ch = 5 px).
* Multiple colour and overlay options.
* Upto 1152 user definable characters, for "better" higher graphics modes.
* Network efficient 16 kB total memory for a full graphics rendition.

RETIDY BELOW THIS LINE LATER

FPC Unit and Application Set
----------------------------

Try `CLIFly help` at the prompt.

* CliFly - A wrapper for CLI functionality and some utilities built in.
  * A generic frame work to add verbs and add tests.
  * A system for allowing version retrograding.
  * Verbs to handle version and verb inclusion tests.
* GenericProcess - A unit with very common for me idioms and functions.
  * Exit and error condition handling.
  * Parameter fetch and error context.
  * Common query and help for CLI tools.
* HardNet - A unit abstraction on the socket layer for me to use easily.
* HardFile - A unit abstraction on the file layer for me to use easily.
  * Various file stream types with exception handling.
* Curses - A unit for console print at and other similar functioning.
* AbstractDataTypes - A unit of some abstract data types to clean code.
* HardwareAbstract - A unit to abstract the hardware so AROS or PC is good.
* HardException - A unit with exception class building and standard messages.
  * Standardized on application exceptions. As opposed to runtime exceptions.
* Help - A unit of helper functions and the internationalization texts.
  * made .rsj, then onto .po and .mo

TODO
----

Ideas for adding in tools are quite numerous. There are some interesting things
along with some avarage and therefore already done things. There should be the
drive to make something advanced, unique and/or convenient, and to avoid too
many un-fun gimmicks. This is a serious tool and framework.

There may be some simple games to test out units, some easy crypto, data compression,
format conversions, archive deltas and checksums, datastore formats, persistant
datatypes and perhaps a simplified script language engine. The choice is wide.

It is likely that tools useful in the bootstrap extension of CliFly will get
developed first. For example, the in-built help may benefit from some compression.

* Build a tag based file format reader based on chunk PNG files.
* Build in some data compression standards.
* Add in conversion to **standardized** file format.
* Build in net transmission and reception of **stadardized** files.
* Create some data compression extensions.
* Do some simple crypto and signature extensions.
* Maybe a CLI game or two.
* Format conversions.
* Increase CRC embedding or chunking options.
* Archive delta formats.
* Maybe some simple video compression testing.
* Data storeage tools.
* Word and spelling analysis tools.
* Stats and grep/awk/sed like?
* Indexing and search retrival tools.
* Html production?
* Web/app framework?
* Simple audio?
* urlencode/decode?
* UTF tools?
* Jar bootstrap?
* Scripting language?
* TurboVision/FreeVision/ncurses tools?
* Persistant object management tools?
* Arty tools?

That's quite a list. Some of it is more basic than other parts, but the next part to do is
the file standardization.

Standard Chunk File
-------------------

Irony makes me want to pick the file extension `*.chk` as this is how many tools would
see the file. The aim is to clone the `*.png` file header, but with a **png** to **chk**
in the 3 bytes where they occur. In fact the general principal would allow subversions
such that any extension replacement just leads to different rules on the hierarchy on
the chunk structure. This is a good pricipal, so I like it.

The next step is then some forward and backward compatibility. This requires some kind
of chunk syntax design. The best plan at present involves nesting and slots. The one instance
in many possible positions kind of chunk is the tricky one to deal with in the syntax.

This is where overly limiting statements like "The IHDR chunk must be the first chunk", are
not good, as it really is more appropriate to have it be before the first IDAT chunk.
Allowing ".png" as a chunk fourCC for example opens up the possibility of then having
the stream follow with a PNG embedded, minus the 8 byte file header, and maybe even
nesting and perhaps interleaving can be used.

The only parameter that would need to be in ".xyz" chunks is a skip chunk count. If
zero the immediate following chunk starts the embedding. If not zero the interleave of
another chunk set comes before the starting embedded xyz chunk. This allows prefetch
of decoders, and interleaving of objects of different extension. Objects with the same
extension would nest recursively (given a correct grammer), but would not interleave.

  -- Jacko.