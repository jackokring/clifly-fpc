{$I head}

UNIT U437;

INTERFACE

TYPE
    UString = UnicodeString; (* for keeping your code shorter and consistent *)

(* there maybe things to do these things behind the scenes, but these leave the string kind
and only operate on the data format *)

(* convert an ansi string into a UTF8 *)
(* also maps up C1 control codes to harmless characters *)
(* non escaped (char 27) control codes (C0 group) will remain as control codes *)
(* escaped control codes will convert to the glyph in UTF8 *)
(* this code page is windows classic codepage *)
FUNCTION ansi(input: String): String;

(* convert a cp437 string into a UTF8 *)
(* maps the classic IBM PC default codepage *)
FUNCTION u437(input: String): String;

(* convert a post modern synthetic code page in to UTF8 *)
FUNCTION ugsi(input: String): String;

(* convert an amiga string to UTF8 from a500 topaz *)
(* also allows more characters *)
FUNCTION topaz(input: String): String;

(* back convert UTF8 to ugsi, as why would you want anything else? *)
FUNCTION makeUgsi(input: String): String;

(* escape control code set of 32, (C0) to access lowest 32 chars and not interpret as control codes *)
(* use before ansi() to get access to lower 32 character glyphs *)
FUNCTION ctrlEscape(input: String): String;

(* maps the C1 control code set to C0 escape sequences *)
(* this frees the code space, to allow ansi character use *)
FUNCTION C1toC0Map(input: UString): UString;

(* like the above but assumes input and output is UTF8 *)
FUNCTION C1toC0MapUTF(input: String): String;

(* generic non casting with no behind the scences spoilers (from and to UTF8) *)
FUNCTION getWide(input: String): UString;
FUNCTION getNarrow(input: UString): String;

(* basic extend width with no coding *)
FUNCTION getWideZero(input: String): UString;

(* chop off upper byte to make narrow string *)
(* returns ISO Latin 1 *)
FUNCTION getNarrowChop(input: UString): String;

(* basic UTF manufacture *)
FUNCTION Char8(input: LongInt): String;
FUNCTION Char16(input: LongInt): UString;

(* basic UTF length of char in string *)
FUNCTION length8(input: String; at: LongInt): Integer;
FUNCTION length16(input: UString; at: LongInt): Integer;

(* decode UTF to codepoint *)
FUNCTION decode8(input: String; at: LongInt): LongInt;
FUNCTION decode16(input: UString; at: LongInt): LongInt;

(* return last residual part of character *)
FUNCTION last8(input: String): String;
FUNCTION last16(input: UString): UString;

IMPLEMENTATION

USES sysutils;

TYPE
    idxR = 0 .. 15;
    idxC = 0 .. 9; (* 3 row saving *)
    idxM = 0 .. 5;
    row = Array [idxR] of UInt16;
    table = Array [idxC] of row;
    minitable = Array [idxM] of row;
    PTable = ^table;
    idxS = 0 .. 127;
    surgets = Array [idxS] of UInt32;

CONST
    page437: table = (
        (* control characters *)
        (  $0, $263A, $263B, $2665,        $2666, $2663, $2660, $2022,
            $2508, $25CB, $2509, $2642,     $2640, $266A, $266B, $263C ),
        (  $25BA, $25C4, $2195, $203C,     $00B6, $00A7, $25AC, $21A8,
            $2191, $2193, $2192, $2190,     $221F, $2194, $25B2, $25BC ),
        (* those 96 chars are very ASCII so not needing change
        to make them so would mean the terminal control codes would not work *)
        (  $C7, $FC, $E9, $E2,       $E4, $E0, $E5, $E7,
            $EA, $EB, $E8, $EF,       $EE, $EC, $C4, $C5 ),
        (  $C9, $E6, $C6, $F4,       $F6, $F2, $FB, $F9,
            $FF, $D6, $DC, $A2,       $A3, $A5, $20A7, $192 ),
        (  $E1, $ED, $F3, $FA,       $F1, $D1, $AA, $BA,
            $BF, $2310, $AC, $BD,       $BC, $A1, $AB, $BB ),
        (  $2591, $2592, $2593, $2502,       $2524, $2561, $2562, $2556,
            $2555, $2563, $2551, $2557,       $2550, $255C, $255B, $2510 ),

        (  $2514, $2534, $252C, $251C,       $2500, $253C, $255E, $255F,
            $255A, $2554, $2569, $2566,       $2560, $2550, $256C, $2567 ),
        (  $2568, $2564, $2565, $2559,       $2558, $2552, $2553, $256B,
            $256A, $2518, $250C, $2588,       $2584, $258C, $2590, $2580 ),
        (  $3B1, $DF, $393, $3C0,          $3A3, $3C3, $B5, $3C4,
            $3A6, $398, $3A9, $3B4,         $221E, $3C6, $3B5, $2229 ),
        (  $2261, $B1, $2265, $2264,       $2320, $2321, $F7, $2248,
            $B0, $2219, $B7, $221A,         $207F, $B2, $25A0, $A0 )
    );

(* plus extras:
    HOP: fast forward.
    RI : recycling.
    SS3: pentagram.
    DCS: sliding car.
    OSC: skull and bones.
*)

(* tables have gaping holes where ASCII standard is *)

    pageansi: table = (
        ( $2400, $2401, $2402, $2403, $2404, $2405, $2406, $2407,
            $2408, $2409, $240A, $240B, $240C, $240D, $240E, $240F ),
        ( $2410, $2411, $2412, $2413, $2414, $2415, $2416, $2417,
            $2418, $2419, $241A, $241B, $241C, $241D, $241E, $241F ),
        (* the first 128 chars are very ASCII so not needing change.
        to make them so would mean the terminal control codes would not work *)
        ( 8364, $23ED, 8218, 402,       8222, 8230, 8224, 8225,       710, 8240, 352, 8249,       338, $2672, 381, $26E4 ),
        ( $26D0, 8216, 8217, 8220,      8221, 8226, 8211, 8212,       732, 8482, 353, 8250,       339, $2620, 382, 376 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) (* 6 blank sets not needing mapping *)
    );

(* the Universal Galactic Standards Institute joke is present, but as a simple character standard? *)
(* this code page is for entertainment purposes only. your milage may vary. let's see how to get a code page number *)

    pageugsi: table = ( (* ASKII ? Well I never had so much in 256 chars *)
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        (* ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),

        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ), *)

        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),

        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 )
    );

    PUA = $DB80; (* a constant to add to the PUA *)

(* of course SI and SO are supposed to be working control codes so why not? *)

    pagesosi: table = ( (* ASKII ? Well I never had so much in 512 chars *)
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ), (* still have to escape lower 32 to use *)
        (* and 3 * 32 *)

        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),

        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 )
    );

    sosiextra: minitable = ( (* covers the three ASCII rows of glyphs *)
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
        ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 )
    );

    surgets4: surgets = ( (* at the high code space $DB80 to $DBFF, replace with the following >>4<< byte codes *)
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    );

    errChr = $2620; (* skull and bones *)

FUNCTION Char8(input: LongInt): String;
VAR
    low, up: Integer;
BEGIN
    Char8 := '';
    IF input < 128 THEN
        Char8 := Char(input) (* no mapping so keep as is *)
    ELSE
        BEGIN (* emit Unicode UTF-8? *)
            low := (input and 63) or 128; (* a trailing byte *)
            up := ((input shr 6) and 63);
            IF input <= $8FF THEN (* 2 byte *)
                BEGIN
                    Char8 := Char(up or 192) + Char(low);
                END
            ELSE IF input <= $FFFF THEN
                BEGIN (* 3 byte *)
                    Char8 := Char((input shr 12) or 224) + Char(up or 128) + Char(low);
                END
            ELSE IF input <= $10FFFF THEN
                BEGIN (* 4 byte *)
                    Char8 := Char(((input shr 18) and 7) or 240) + Char(((input shr 12) and 63) or 128)
                         + Char(up or 128) + Char(low);
                    (* check bounds range 21 bit max *)
                END
            ELSE
                char8 := Char8(errChr); (* nest *)
        END;
END;

FUNCTION Char16(input: LongInt): UString;
BEGIN
    IF input <= $FFFF THEN
        Char16 := WideChar(input)
    ELSE IF input <= $10FFFF THEN
        BEGIN (* hi/lo surragate pair *)
            input := input - $10000;
            Char16 := WideChar(((input shr 10) and $3FF) or $D800) + WideChar((input and $3FF) or $DC00);
        END
    ELSE
        Char16 := Char16(errChr);
END;

FUNCTION mapChar(input: Char; using: PTable; escape: Boolean): String;
VAR
    up: Integer;
    top: LongInt;
BEGIN
    mapChar := '';
    up := Integer(input) shr 4;
    IF (up < 2) and not(escape) THEN
        (* send escaped control codes as though they should be the characters displayed? *)
        BEGIN
            mapChar := input; (* control code pass through *)
            EXIT;
        END;
    IF (up >= 2) and (up <= 7) THEN (* specials *)
        IF using = @pagesosi THEN
            BEGIN
                top := sosiextra[up - 2][Integer(input) and 15]; (* specials *)
            END
        ELSE
            BEGIN
                top := 0; (* ASCII main block *)
            END
    ELSE (* not special so map *)
        BEGIN
            IF up > 2 THEN
                up := up - 6; (* map up  by reduced table *)
            top := using^[up][Integer(input) and 15];
        END;
    IF top = 0 THEN
        top := Integer(input); (* self map *)
    IF (top >= $DB80) and (top <= $DBFF) THEN (* PUA surragates mapping to 4 byte codes *)
        top := surgets4[top - $DB80];
    mapChar := Char8(top);
    IF escape and (Integer(input) < 32) THEN (* CAN is not the right code to use here *)
        mapChar := Char(26) + mapChar; (* add SUB to cancel escape *)
END;

FUNCTION change(input: String; using: PTable): String;
VAR
    i: LongInt;
    c: Char;
    l: Boolean = False;
BEGIN
    change := '';
    IF length(input) = 0 THEN EXIT;
    FOR i := 1 TO length(input) DO
        BEGIN
            c := input[i];
            (* synthetic SO SI support *)
            IF (using = @pageugsi) and (c = Char(14)) and not(l) THEN (* SO *)
                BEGIN
                    using := @pagesosi;
                    CONTINUE;
                END;
            IF (using = @pagesosi) and (c = Char(15)) and not(l) THEN (* SI *)
                BEGIN
                    using := @pageugsi;
                    CONTINUE;
                END;
            change := change + mapChar(c, using, l); (* delayed escape *)
        END;
    l := (Integer(c) = 27);
END;

FUNCTION ansi(input: String): String;
BEGIN
    ansi := change(input, @pageansi); (* added shaded on DEL for amiga compatability *)
END;

FUNCTION u437(input: String): String;
BEGIN
    u437 := change(input, @page437);
END;

FUNCTION ugsi(input: String): String;
BEGIN
    ugsi := change(input, @pageugsi);
END;

FUNCTION topaz(input: String): String;
BEGIN
    topaz := change(input, @pageansi); (* almost identical, with shaded on DEL *)
END;

FUNCTION ctrlEscape(input: String): String;
VAR
    i: LongInt;
BEGIN
    ctrlEscape := '';
    IF length(input) = 0 THEN EXIT;
    FOR i := 1 TO length(input) DO
        BEGIN
            IF Integer(input[i]) < 32 THEN
                ctrlEscape := ctrlEscape + Char(27);
            ctrlEscape := ctrlEscape + input[i];
        END;
END;

FUNCTION C1toC0Map(input: UString): UString;
VAR
    i: LongInt;
BEGIN
    C1toC0Map := '';
    IF length(input) = 0 THEN EXIT;
    FOR i := 1 TO length(input) DO
        BEGIN
            IF (Integer(input[i]) and $FFE0) = $80 THEN (* a C1 code *)
                BEGIN
                    C1toC0Map := C1toC0Map + WideChar(27);
                    C1toC0Map := C1toC0Map + WideChar((Integer(input[i]) and $1F) or 64); (* second character is upper case *)
                END
            ELSE
                C1toC0Map := C1toC0Map + input[i];
        END;
END;

FUNCTION C1toC0MapUTF(input: String): String;
VAR
    i: LongInt;
    h: Boolean = False;
BEGIN
    C1toC0MapUTF := '';
    IF length(input) = 0 THEN EXIT;
    FOR i := 1 TO length(input) DO
        BEGIN
            IF Integer(input[i]) = $C2 THEN (* a C1 code 11000010 100xxxxx *)
                BEGIN
                    C1toC0MapUTF := C1toC0MapUTF + Char(27);
                    h := True;
                END
            ELSE
                BEGIN
                    IF h THEN
                        IF (Integer(input[i]) and $20) = 0 THEN
                            C1toC0MapUTF := C1toC0MapUTF + Char((Integer(input[i]) and $1F) or 64) (* second character is upper case *)
                        ELSE
                            BEGIN
                                C1toC0MapUTF := C1toC0MapUTF + Char(26); (* SUB *)
                                C1toC0MapUTF := C1toC0MapUTF + input[i];
                            END
                    ELSE
                        C1toC0MapUTF := C1toC0MapUTF + input[i];
                    h := False;
                END;
        END;
END;

FUNCTION length8(input: String; at: LongInt): Integer;
BEGIN
    IF (at > length(input)) or (at < 1) THEN
        BEGIN
            length8 := 0;
            EXIT;
        END;
    IF Integer(input[at]) < 192 THEN (* ascii or floating exteneded *)
        length8 := 1
    ELSE IF Integer(input[at]) < 224 THEN (* len 2 *)
        length8 := 2
    ELSE IF Integer(input[at]) < 240 THEN (* len 3 *)
        length8 := 3
    ELSE IF Integer(input[at]) < 248 THEN (* len 4 *)
        length8 := 4
    ELSE
        length8 := 1; (* floating unused code *)
END;

FUNCTION length16(input: UString; at: LongInt): Integer;
BEGIN
    IF (at > length(input)) or (at < 1) THEN
        BEGIN
            length16 := 0;
            EXIT;
        END;
    IF (Integer(input[at]) and $FC00) = $D800 THEN (* surragate hi *)
        length16 := 2
    ELSE
        length16 := 1;
END;

FUNCTION chkExt(input: String; at: LongInt; cnt: Integer): Boolean;
VAR
    i: LongInt;
    b: Boolean = True;
BEGIN
    FOR i := (at + 1) TO (at + cnt - 1) DO
        IF (Integer(input[i]) and $C0) <> $80 THEN
            b := False; (* not ok *)
END;

FUNCTION decode8(input: String; at: LongInt): LongInt;
VAR
    i: Integer;
BEGIN
    IF (at > length(input)) or (at < 1) THEN
        BEGIN
            decode8 := errChr;
            EXIT;
        END;
    i := length8(input, at); (* get length *)
    IF i = 1 THEN
        IF Integer(input[at]) >= 248 THEN (* error *)
            decode8 := errChr
        ELSE IF Integer(input[at]) < 128 THEN (* ok *)
            decode8 := Integer(input[at])
        ELSE (* floating extend *)
            decode8 := errChr
    ELSE IF (at + i) > length(input) THEN (* not an error *)
        decode8 := -1
    ELSE IF chkExt(input, at, i) THEN
        BEGIN (* ok *)
            IF i = 2 THEN
                decode8 := ((Integer(input[at]) and $1F) shl 6) or (Integer(input[at + 1]) and $3F)
            ELSE IF i = 3 THEN
                decode8 := ((Integer(input[at]) and $F) shl 12) or ((Integer(input[at + 1]) and $3F) shl 6)
                    or (Integer(input[at + 2]) and $3F)
            ELSE (* i = 4 *)
                decode8 := ((Integer(input[at]) and $7) shl 18) or ((Integer(input[at + 1]) and $3F) shl 12)
                    or ((Integer(input[at + 2]) and $3F) shl 6) or (Integer(input[at + 3]) and $3F);
        END
    ELSE
        decode8 := errChr; (* bad following extends *)
END;

FUNCTION decode16(input: UString; at: LongInt): LongInt;
BEGIN
    IF (at > length(input)) or (at < 1) THEN
        BEGIN
            decode16 := errChr;
            EXIT;
        END;
    IF (Integer(input[at]) and $FC00) = $D800 THEN (* surragate hi *)
        IF (at + 1 > length(input)) THEN
            decode16 := -1 (* not technically an error *)
        ELSE
            BEGIN (* check for lower surragate *)
                IF (Integer(input[at + 1]) and $FC00) = $DC00 THEN (* surragate lo *)
                    decode16 := errChr
                ELSE
                    decode16 := ((Integer(input[at]) and $3FF) shl 10) or (Integer(input[at + 1]) and $3FF);
            END
    ELSE
        IF (Integer(input[at]) and $FC00) = $DC00 THEN (* surragate lo *)
            decode16 := errChr
        ELSE
            decode16 := Integer(input[at]);
END;

FUNCTION last8(input: String): String;
VAR
    code: LongInt;
BEGIN
    IF length(input) = 0 THEN
        BEGIN
            last8 := '';
            EXIT;
        END;
    IF Integer(input[length(input)]) < 128 THEN
        BEGIN
            last8 := '';
            EXIT;
        END;
    (* must be a code of length 2 or more *)
    code := length(input);
    WHILE (length8(input, code) < 2) and (code >= (length(input) - 3))
        and chkExt(input, code, length(input) - code) DO (* not found a start *)
        BEGIN
            code := code - 1;
        END;
    (* too long, or errored extensions *)
    IF (code < (length(input) - 3)) or chkExt(input, code, length(input) - code) THEN (* not found *)
        BEGIN
            last8 := '';
            EXIT;
        END;
    (* code found now check valid options *)
    IF (length8(input, code) + code - 1) > length(input) THEN (* found last *)
        last8 := Copy(input, code, length8(input, code)) (* copy part of char *)
    ELSE
        last8 := ''; (* last is complete *)
END;

FUNCTION last16(input: UString): UString;
BEGIN
    IF length(input) = 0 THEN
        BEGIN
            last16 := '';
            EXIT;
        END;
    IF (Integer(input[length(input)]) and $FC00) = $D800 THEN
        last16 := input[length(input)]
    ELSE
        last16 := '';
END;

FUNCTION getWide(input: String): UString;
VAR
    at: LongInt = 1;
    code: LongInt;
BEGIN
    getWide := '';
    WHILE length8(input, at) > 0 DO
        BEGIN
            code := decode8(input, at);
            IF code < 0 THEN
                EXIT;
            getWide := getWide + Char16(code);
            at := at + length8(input, at);
        END;
    (* getWide := UTF8Decode(input); *)
END;

FUNCTION getNarrow(input: UString): String;
VAR
    at: LongInt = 1;
    code: LongInt;
BEGIN
    getNarrow := '';
    WHILE length16(input, at) > 0 DO
        BEGIN
            code := decode16(input, at);
            IF code < 0 THEN
                EXIT;
            getNarrow := getNarrow + Char8(code);
            at := at + length16(input, at);
        END;
    (* getNarrow := UTF8Encode(input); *)
END;

FUNCTION getWideZero(input: String): UString;
VAR
    i: LongInt;
BEGIN
    getWideZero := '';
    IF length(input) = 0 THEN EXIT;
    FOR i := 1 TO length(input) DO
        getWideZero := getWideZero + WideChar(input[i]);
END;

FUNCTION getNarrowChop(input: UString): String;
VAR
    i: LongInt;
BEGIN
    getNarrowChop := '';
    IF length(input) = 0 THEN EXIT;
    FOR i := 1 TO length(input) DO
        IF Integer(input[i]) > 255 THEN
            CONTINUE (* maybe a fix here later *)
        ELSE
            getNarrowChop := getNarrowChop + Char(input[i]);
END;

FUNCTION mapUgsi(input: LongInt): String;
VAR
    up: LongInt;
    i, j, k: Integer;
BEGIN
    up := input shr 16;
    (* check ASKII *)
    IF (up < 8) THEN
        BEGIN
            mapUgsi := Char(input);
            EXIT;
        END;
    (* check surragate4 *)
    FOR i := 1 TO 128 DO
        IF surgets4[i] = input THEN
            BEGIN
                input := i + PUA; (* get the indirect used *)
                BREAK;
            END;
    (* check main table *)
    (* there will be no zero codes *)
    FOR i := 1 TO 16 DO
        FOR j := 1 TO 10 DO
            BEGIN
                IF pageugsi[j][i] = input THEN
                    BEGIN
                        k := j - 1;
                        IF k >= 2 THEN k := k + 6;
                        IF j < 2 THEN
                            mapUgsi := Char(27) + Char(k * 16 + i - 1)
                        ELSE
                            mapUgsi := Char(k * 16 + i - 1);
                        BREAK;
                    END;
                IF pagesosi[j][i] = input THEN
                    BEGIN
                        k := j - 1;
                        IF k >= 2 THEN k := k + 6;
                        IF j < 2 THEN
                            mapUgsi := Char(14) + Char(27) + Char(k * 16 + i - 1) + Char(15)
                        ELSE
                            mapUgsi := Char(14) + Char(k * 16 + i - 1) + Char(15);
                        BREAK;
                    END;
            END;
    FOR i := 1 TO 16 DO
        FOR j := 1 TO 6 DO
            BEGIN
                IF sosiextra[j][i] = input THEN
                    BEGIN
                        mapUgsi := Char(14) + Char((j + 1) * 16 + i - 1) + Char(15);
                        BREAK;
                    END;
            END;
END;

FUNCTION makeUgsi(input: String): String;
VAR
    at: LongInt = 1;
    code: LongInt;
BEGIN
    makeUgsi := '';
    WHILE length8(input, at) > 0 DO
        BEGIN
            code := decode8(input, at);
            IF code < 0 THEN
                EXIT;
            makeUgsi := makeUgsi + mapUgsi(code);
            at := at + length8(input, at);
        END;
END;

INITIALIZATION
    SetMultiByteConversionCodePage(CP_UTF8); (* default code page for device ANSI basis *)
    SetMultiByteFileSystemCodePage(CP_UTF8);
    SetMultiByteRTLFileSystemCodePage(CP_UTF8);

END.
