{$I head}

UNIT HardException;
(* this unit is for placing all the hardend exception routines (for error recovery) in
as this cleans up the code of all future applications so as to not get messy *)

INTERFACE

USES Classes, SysUtils, Help;

TYPE
    PossibleErrors = (
        FormatEx, ConsumerEx, ProducerEx, NameEx,
        AmbiguityEx, ProxyEx, OriginEx, LimitEx,
        FinalEx, IdentityEx, DecoderEx, CoachEx
    );
    MajorStrings = Array [PossibleErrors] of String;

CONST
    DisplayErrors: MajorStrings = (
        'Format', 'Consumer', 'Producer', 'Name',
        'Ambiguity', 'Proxy', 'Origin', 'Limit',
        'Final', 'Identity', 'Decoder', 'Coach'
    );

    HelpTextException: MajorStrings = (
        FormatExHelp, ConsumerExHelp, ProducerExHelp, NameExHelp,
        AmbiguityExHelp, ProxyExHelp, OriginExHelp, LimitExHelp,
        FinalExHelp, IdentityExHelp, DecoderExHelp, CoachExHelp
    );

TYPE
    AppException = CLASS(Exception) (* application exception base class *)
        PROTECTED
            chain: AppException;
            hi: PossibleErrors;
            FUNCTION hc(): LongInt;
            FUNCTION pretty(): String;
            FUNCTION RdEx(p: PossibleErrors): AppException;
        PUBLIC
            CONSTRUCTOR Create(what: String; p: PossibleErrors);
            DESTRUCTOR Destroy(); VIRTUAL;
            PROPERTY HelpContext: LongInt READ hc;
            PROPERTY Message: String READ pretty;
            PROPERTY Assign[p: PossibleErrors]: AppException READ RdEx; DEFAULT;
    END;

    ExitException = CLASS(Exception) (* default to exit *)
        PUBLIC
            CONSTRUCTOR Create(m: String);
        PROTECTED
            FUNCTION RdEx(m: String): ExitException;
        PUBLIC
            PROPERTY Assign[m: String]: ExitException READ RdEx; DEFAULT;
    END;

    WarningException = CLASS(ExitException); (* default to exit *)
    (*    PUBLIC
            CONSTRUCTOR Create();
    END; *)

    ErrorException = CLASS(ExitException); (* default to exit *)
    (*    PUBLIC
            CONSTRUCTOR Create();
    END; *)

    FailException = CLASS(ExitException); (* default to exit *)
    (*    PUBLIC
            CONSTRUCTOR Create();
    END; *)

VAR
    AppEx: AppException;
    ExitEx: ExitException;
    WarningEx: ExitException;
    ErrorEx: ExitException;
    FailEx: ExitException;

IMPLEMENTATION

FUNCTION ExitException.RdEx(m: String): ExitException;
BEGIN
    RdEx := ExitException.Create(m);
END;

CONSTRUCTOR ExitException.Create(m: String);
BEGIN
    INHERITED Create(m);
END;

CONSTRUCTOR AppException.Create(what: String; p: PossibleErrors);
BEGIN
    INHERITED Create(what);
    hi := p;
END;

FUNCTION AppException.RdEx(p: PossibleErrors): AppException;
BEGIN
    RdEx := AppException.Create(DisplayErrors[p], p);
    RdEx.chain := Self;
END;

FUNCTION AppException.hc(): LongInt;
BEGIN
    hc := 0;
END;

FUNCTION AppException.pretty(): String;
BEGIN
    pretty := INHERITED Message + 'Exception: (' + HelpTextException[hi] + ')';
    IF chain <> Nil THEN
        pretty := pretty + ' | ' + chain.Message;
    (* this should print the encapsulation *)
END;

DESTRUCTOR AppException.Destroy();
BEGIN
    IF chain <> Nil THEN
        chain.Free;
    INHERITED;
END;

CONST
        blank = 'Blank';

INITIALIZATION
    AppEx := AppException.Create(DisplayErrors[FinalEx], FinalEx); (* an empty exception *)
    ExitEx := ErrorException.Create(blank);
    WarningEx := WarningException.Create(blank);
    ErrorEx := ErrorException.Create(blank);
    FailEx := FailException.Create(blank);

FINALIZATION
    AppEx.Free;
    ExitEx.Free;
    WarningEx.Free;
    ErrorEx.Free;
    FailEx.Free;

END.
