{$I head}

UNIT HardFile;
(* this unit is for placing all the hardend file io routines (with error recovery) in
as this cleans up the code of all future applications so as to not get messy *)

(*
RawFile > PipeFile > LogFile

*)
INTERFACE

USES Classes, Pipes, SysUtils, HardException, Help;

TYPE
        FileMode = (RWFileMode, RFileMode, WFileMode, RCacheFileMode,
            WCacheBackFileMode, RWCacheThruFileMode, RWCacheBackFileMode);
        FileModeAccess = Array [FileMode] of Word;

CONST
{$IFDEF WINDOWS}
        logFileName = '{LocalAppData}\clifly.log'; (* for some reason the windows goes funny *)
{$ELSE}
{$IFDEF HASAMIGA}
        logFileName = 'SYS:CLIFly.log';
{$ELSE}
        logFileName = '/var/log/clifly.log';
{$ENDIF}
{$ENDIF}

        FileModeConstants: FileModeAccess = (
        (* simpler mode options for files *)
            fmOpenReadWrite or fmShareDenyWrite,
            fmOpenRead or fmShareDenyNone,
            fmOpenWrite or fmShareDenyWrite,
        (* and cache modes for later *)
            fmOpenRead or fmShareDenyWrite,
            fmOpenWrite or fmShareExclusive,
            fmOpenReadWrite or fmShareDenyWrite,
            fmOpenReadWrite or fmShareExclusive
        );

TYPE
    RawConsole = CLASS(TStream)
        PROTECTED
            PROCEDURE SetSize(CONST NewSize: Int64); OVERRIDE;
            FUNCTION GetSize(): Int64; OVERRIDE;
            FUNCTION GetPosition(): Int64; OVERRIDE;
            PROCEDURE SetPosition(CONST NewPosition: Int64); OVERRIDE;
        PUBLIC
            FUNCTION Read(VAR Buffer; Count: LongInt): LongInt; OVERRIDE;
            FUNCTION Write(CONST Buffer; Count: LongInt): LongInt; OVERRIDE;
            CONSTRUCTOR Create();
            DESTRUCTOR Destroy(); OVERRIDE;
            FUNCTION Seek(CONST Offset: Int64; Origin: TSeekOrigin): Int64; OVERRIDE;
        PROTECTED
            keys: TInputPipeStream;
            disp: TOutputPipeStream;
    END;

    (* it seems impossible to inherit constructors without writing them out again *)

    RawFile = CLASS(TFileStream)
        PROTECTED
            Temp: Boolean;
        PUBLIC
            FUNCTION Read(VAR Buffer; Count: LongInt): LongInt; OVERRIDE;
            FUNCTION Write(CONST Buffer; Count: LongInt): LongInt; OVERRIDE;
            CONSTRUCTOR Create();
            CONSTRUCTOR Create(CONST AFileName: String; Mode: FileMode);
            CONSTRUCTOR Create(CONST AFileName: String);
            DESTRUCTOR Destroy(); OVERRIDE;
            FUNCTION Seek(CONST Offset: Int64; Origin: TSeekOrigin): Int64; OVERRIDE;
            FUNCTION ReadRaw(CONST Count: LongInt): String;
            PROCEDURE WriteRaw(CONST Buffer: String);
    END;

    PipeFile = CLASS(RawFile) (* or que file, where the reader can read anything wrote at a differing location *)
        PUBLIC
            ReadPosition: Int64; (* where the read comes from *)
            FUNCTION Read(VAR Buffer; Count: LongInt): LongInt; (* VIRTUAL; *) OVERRIDE;
            CONSTRUCTOR Create(CONST AFileName: String; Mode: FileMode);
            CONSTRUCTOR Create();
            CONSTRUCTOR Create(CONST AFileName: String);
    END;

    LogFile = CLASS(PipeFile) (* or stack file, for easy tail reading without issues? *)
        PUBLIC
            CONSTRUCTOR Create(CONST AFileName: String; Mode: FileMode);
            CONSTRUCTOR Create(); (* does not need OVERRIDE (or VIRTUAL as class method), no param temp. *)
            CONSTRUCTOR Create(CONST AFileName: String);
            FUNCTION Read(VAR Buffer; Count: LongInt): LongInt; (* VIRTUAL; *) OVERRIDE;
            FUNCTION Write(CONST Buffer; Count: LongInt): LongInt; (* VIRTUAL; *) OVERRIDE;
    END;

VAR
    Logger: LogFile; (* unit initialized log *)

PROCEDURE makeLog(CONST str: String); (* log those strings *)
FUNCTION readLog(CONST number, width: Integer): String;

IMPLEMENTATION

FUNCTION readLog(CONST number, width: Integer): String;
VAR
    i: Integer;
BEGIN
    readLog := '';
    FOR i := 1 TO number DO
        TRY
            readLog := readLog + Logger.ReadRaw(width) + LineEnding;
        EXCEPT
            readLog := readLog + '[No more readable lines.]' + LineEnding;
        END;
END;

PROCEDURE makeLog(CONST str: String);
BEGIN
    Logger.WriteRaw(str); (* the length is kept on the back chain *)
END;

//=============================================================

CONSTRUCTOR LogFile.Create(CONST AFileName: String; Mode: FileMode);
BEGIN
    INHERITED;
    ReadPosition := Size; (* starts at end *)
END;

FUNCTION LogFile.Read(VAR Buffer; Count: LongInt): LongInt;
VAR
    ptr: LongInt;
BEGIN
    ReadPosition := ReadPosition - sizeOf(LongInt);
    IF INHERITED Read(ptr, sizeOf(LongInt)) < sizeOf(LongInt) THEN
        RAISE AppEx[ConsumerEx];
    IF Count > ptr THEN
        Count := ptr;
    ReadPosition := ReadPosition - sizeOf(LongInt) - ptr;
    IF (ReadPosition < 0) or (ReadPosition > Size) THEN
        RAISE AppEx[FormatEx];
    Read := INHERITED Read(Buffer, Count);
    ReadPosition := ReadPosition - Count;
END;

FUNCTION LogFile.Write(CONST Buffer; Count: LongInt): LongInt;
BEGIN
    Position := Size; (* prepare for writing *)
    Write := INHERITED Write(Buffer, Count);
    Write := Write + INHERITED Write(NtoBE(Write), sizeOf(LongInt));
    IF Write < (Count + sizeOf(LongInt)) THEN
        BEGIN
            Size := (Size - Write); (* rid of bad *)
            RAISE AppEx[ProducerEx]; (* should not fail *)
        END
    ELSE
        Write := Write - sizeOf(LongInt);
END;

CONSTRUCTOR LogFile.Create(CONST AFileName: String);
BEGIN
    Create(AFileName, RWFileMode); (* default make and full recreate *)
END;

CONSTRUCTOR LogFile.Create();
BEGIN
    INHERITED Create();
    Position := Size; (* read possible if write seek at end *)
END;

//===================================================================

FUNCTION PipeFile.Read(VAR Buffer; Count: LongInt): LongInt;
VAR
    p: Int64;
BEGIN
    p := Position; (* save writing position *)
    IF (ReadPosition >= p) or (ReadPosition < 0) THEN
        RAISE AppEx[LimitEx] (* can't read after *)
    ELSE
        BEGIN
            IF ReadPosition + Count >= p THEN
                Count := p - ReadPosition - 1; (* amount available, the -1 for the write slot *)
            Position := ReadPosition;
            Read := INHERITED Read(Buffer, Count);
            Position := p; (* restore *)
            ReadPosition := ReadPosition + Count;
        END;
END;

CONSTRUCTOR PipeFile.Create(CONST AFileName: String; Mode: FileMode);
BEGIN
    INHERITED;
    ReadPosition := 0; (* initialize where to read from *)
    Position := Size; (* read possible if write seek at end *)
END;

CONSTRUCTOR PipeFile.Create(CONST AFileName: String);
BEGIN
    Create(AFileName, RWFileMode); (* default make and full recreate *)
END;

CONSTRUCTOR PipeFile.Create();
BEGIN
    INHERITED Create();
    ReadPosition := 0; (* initialize where to read from *)
    Position := Size; (* read possible if write seek at end *)
END;

//===================================================================

FUNCTION RawFile.Read(VAR Buffer; Count: LongInt): LongInt;
BEGIN
    TRY
        Read := INHERITED;
    EXCEPT
        RAISE AppEx[ConsumerEx];
    END;
END;

FUNCTION RawFile.Write(CONST Buffer; Count: LongInt): LongInt;
BEGIN
    TRY
        Write := INHERITED;
    EXCEPT
        RAISE AppEx[ProducerEx];
    END;
END;

FUNCTION RawFile.Seek(CONST Offset: Int64; Origin: TSeekOrigin): Int64;
BEGIN
    TRY
        Seek := INHERITED Seek(Offset, Origin);
    EXCEPT
        RAISE AppEx[LimitEx];
    END;
END;

CONSTRUCTOR RawFile.Create(CONST AFileName: String; Mode: FileMode);
BEGIN
    Temp := False;
    TRY
        IF not((Mode = RFileMode) or (Mode = RCacheFileMode)) THEN (* read only modes don't create *)
            IF not(FileExists(AFileName)) THEN
                BEGIN
                    FileClose(FileCreate(AFileName)); (* make a new one for writing *)
                END;
        INHERITED Create(AFileName, FileModeConstants[Mode]);
    EXCEPT
        RAISE AppEx[NameEx];
    END;
END;

FUNCTION RawFile.ReadRaw(CONST Count: LongInt): String;
VAR
    s: String;
    i: LongInt;
BEGIN
    SetLength(s, Count);
    i := Read(s[1], Count);
    SetLength(s, i);
    ReadRaw := s;
    IF i < Count THEN
        BEGIN
            Position := Position - i; (* reset to redo *)
            RAISE AppEx[ConsumerEx];
        END;
END;

PROCEDURE RawFile.WriteRaw(CONST Buffer: String);
VAR
    p: Integer;
BEGIN
    p := Write(Buffer[1], length(Buffer));
    IF p < length(Buffer) THEN
        BEGIN
            Position := Position - p; (* reset to redo *)
            RAISE AppEx[ProducerEx];
        END;
END;

CONSTRUCTOR RawFile.Create(CONST AFileName: String);
BEGIN
    Create(AFileName, RWFileMode); (* default make and full recreate *)
END;

CONSTRUCTOR RawFile.Create();
BEGIN
    Create(GetTempFileName(GetTempDir(True), FormatDateTime('yyyy-mm-dd-hh-nn-ss-', nowUTC())), RWFileMode);
    Temp := True;
END;

DESTRUCTOR RawFile.Destroy();
BEGIN
    TRY
        INHERITED; (* whatever which may need a valid file *)
        IF Temp THEN DeleteFile(FileName); (* then clean up *)
    EXCEPT
        RAISE AppEx[FinalEx];
    END;
END;

//====================================================================

CONSTRUCTOR RawConsole.Create();
BEGIN
    TRY
        INHERITED;
        keys := TInputPipeStream.Create(stdInputHandle);
        disp := TOutputPipeStream.Create(stdOutputHandle);
    EXCEPT
        RAISE AppEx[NameEx]; (* bad name for console? *)
    END;
END;

DESTRUCTOR RawConsole.Destroy();
BEGIN
    TRY
        keys.Free();
        disp.Free();
        INHERITED;
    EXCEPT
        RAISE AppEx[FinalEx];
    END;
END;

FUNCTION RawConsole.Read(VAR Buffer; Count: LongInt): LongInt;
VAR
    i: LongInt;
BEGIN
    TRY
        i := keys.NumBytesAvailable;
            IF i < Count THEN
            Count := i;
        Read := keys.Read(Buffer, Count);
    EXCEPT
        RAISE AppEx[ConsumerEx];
    END;
END;

FUNCTION RawConsole.Write(CONST Buffer; Count: LongInt): LongInt;
BEGIN
    TRY
        Write := disp.Write(Buffer, Count);
    EXCEPT
        RAISE AppEx[ProducerEx];
    END;
END;

PROCEDURE RawConsole.SetSize(CONST NewSize: Int64);
VAR
    b: Byte;
    i: LongInt;
BEGIN
    TRY
        i := keys.NumBytesAvailable;
        WHILE i > NewSize DO
            ReadByte();
    EXCEPT
        RAISE AppEx[LimitEx]; (* should not fail *)
    END;
END;

FUNCTION RawConsole.GetSize(): Int64;
BEGIN
    GetSize := keys.NumBytesAvailable;
END;

FUNCTION RawConsole.GetPosition(): Int64;
BEGIN
    GetPosition := 0;
END;

PROCEDURE RawConsole.SetPosition(CONST NewPosition: Int64);
BEGIN
    IF NewPosition < GetSize() THEN
        SetSize(GetSize() - NewPosition);
END;

FUNCTION RawConsole.Seek(CONST Offset: Int64; Origin: TSeekOrigin): Int64;
BEGIN
    RAISE AppEx[LimitEx];
END;

INITIALIZATION
    Logger := LogFile.Create(logFileName);

FINALIZATION
    Logger.Free;

END.
