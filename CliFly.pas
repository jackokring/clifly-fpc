{$I head}

PROGRAM CLIFly;
(* just for some wonder? what about common VAR sections on multiple procedures for the common default lower stack? *)

USES GenericProcess (* my own generics *), Help;

(* forward declarations for pointers *)

CONST
        verbs: MainTable = (
                (v: 'version';  f: 2;   p: @printVersion;       t: @nullTest;
                a: '';          x: normal; h:VersionHelp),         (* OK *)
                (v: 'test';     f: 2;   p: @testOne;            t: @nullTest;
                a: '<verb>';    x: normal; h:testHelp),         (* OK *)
                (v: 'tests';    f: 2;   p: @tests;              t: @nullTest;
                a: '';          x: normal; h:testsHelp),         (* OK *)
                (v: 'help';     f: 2;   p: @printUsage;         t: @nullTest;
                a: '[<verb> ...]';
                                x: normal; h:helpHelp),         (* OK *)
                (v: 'has';      f: 2;   p: @hasVersion;         t: @nullTest;
                a: '<verb> [<verb> ...]';
                                x: normal; h:hasHelp),         (* OK *)
                (v: 'is';       f: 2;   p: @checkVersion;       t: @nullTest;
                a: '<version>'; x: normal; h:isHelp),         (* OK *)
                (v: 'older';    f: 2;   p: @older;              t: @nullTest;
                a: '<version> <verb> [<arg> ...]';
                                x: normal; h:olderHelp),         (* OK *)
                (* older verbs start here *)
                (v: 'older';    f: 1;   p: @nullProc;           t: @nullTest;
                a: '';          x: firstOlder; h:oldestHelp)          (* OK *)
        );

(* the main declarations *)
BEGIN
        start(verbs);
END.

