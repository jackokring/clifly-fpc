{#####################################################################################}
{##                                                                                 ##}
{## ABC - Advanced Blocksorting Compressor                                          ##}
{##                                                                                 ##}
{## ABC_Global_Unit                                                                 ##}
{##                                                                                 ##}
{## Global constants and type definitions                                           ##}
{##                                                                                 ##}
{## Copyright (C) 2002-2003 J�rgen Abel                                             ##}
{##                                                                                 ##}
{#####################################################################################}



Unit ABC_Global_Unit;


Interface


Const
  { Program information }
  VERSION_TEXT                            = '2.4';


  { Program parameter }
  ABC_RLE_EXP_IF_THRESHOLD                = 2;                      { Threshold for IF-RLE }
  ABC_RLE_BIT_AWFC_THRESHOLD              = 2;                      { Threshold for AWFC-RLE }
  ABC_SIF_THRESHOLD                       = 256 * 1024;             { Threshold for SIF }


  { IO }
  ABC_IO_START_TEXT_N                     = 8;
  ABC_A_IO_START_TEXT : Array [0 .. ABC_IO_START_TEXT_N - 1] of String =
    (
      '____________________________________________________________________',
      '',
      'ABC - Advanced Blocksorting Compressor',
      'Version                   : ' + VERSION_TEXT,
      'Copyright (c) 2002-2003   : Juergen Abel',
      'Internet                  : www.data-compression.info',
      '____________________________________________________________________',
      ''
    );
  ABC_IO_HELP_TEXT_N                      = 15;
  ABC_A_IO_HELP_TEXT : Array [0 .. ABC_IO_HELP_TEXT_N - 1] of String =
    (
      'Usage: abc.exe [-hcev1] [input filename/wildcard] [output extension]',
      '  -h : print this help message',
      '  -c : compress files',
      '  -e : expand files',
      '  -v : verbose',
      '  -1 : fast',
      '',
      'If no output extension is given:',
      ' "abc" is default for compression',
      ' "exp" is default for expansion',
      '',
      'Examples:',
      '  abc.exe -c1 autoexec.bat abc',
      '  abc.exe -e  autoexec.abc bat',
      '  abc.exe -cv *.exe        abc'
    );
  ABC_IO_OPTION_START                     = '-';
  ABC_IO_OPTION_HELP                      = 'h';
  ABC_IO_OPTION_COMPRESS                  = 'c';
  ABC_IO_OPTION_EXPAND                    = 'e';
  ABC_IO_OPTION_VERBOSE                   = 'v';
  ABC_IO_OPTION_FAST                      = '1';
  ABC_IO_DELIMITER                        = '.';
  ABC_IO_DEFAULT_COMPRESS_EXTENSION       = 'abc';
  ABC_IO_DEFAULT_EXPAND_EXTENSION         = 'exp';
  ABC_IO_COMPRESS_TEXT                    = 'Compressing ';
  ABC_IO_EXPAND_TEXT                      = 'Expanding ';
  ABC_IO_BETWEEN_TEXT                     = ' to ';
  ABC_IO_MTF_COMPRESS_TEXT                = 'stages                    : BWT - RLE_EXP - MTF - AC';
  ABC_IO_SIF_COMPRESS_TEXT                = 'stages                    : BWT - RLE_EXP - SIF - AC';
  ABC_IO_AWFC_COMPRESS_TEXT               = 'stages                    : BWT - RLE_BIT0 - AWFC - RLE_BIT1 - AC';
  ABC_IO_MTF_EXPAND_TEXT                  = 'stages                    : AC - MTF - RLE_EXP BWT';
  ABC_IO_SIF_EXPAND_TEXT                  = 'stages                    : AC - SIF - RLE_EXP BWT';
  ABC_IO_AWFC_EXPAND_TEXT                 = 'stages                    : AC - RLE_BIT1 - AWFC - RLE_BIT0 - BWT';
  ABC_IO_INPUT_SIZE_TEXT                  = 'Input size                : %13.0n bytes';
  ABC_IO_OUTPUT_SIZE_TEXT                 = 'Output size               : %13.0n bytes';
  ABC_IO_COMPRESSION_RATIO_TEXT           = 'Compression rate          : %17.3f bps';
  ABC_IO_COMPRESSION_TIME_TEXT            = 'Compression time          : %17.3f s';
  ABC_IO_EXPANSION_TIME_TEXT              = 'Expansion time            : %17.3f s';
  ABC_IO_CRC_VALID                        = 'CRC is valid';
  ABC_IO_CRC_NOT_VALID                    = 'CRC is not valid. Program execution terminated!';
  ABC_IO_COMPRESS_START_TEXT              = 'Compressing %4.0n files  ';
  ABC_IO_EXPAND_START_TEXT                = 'Expanding %4.0n files  ';
  ABC_IO_FILE_NOT_FOUND                   = 'File not found. Program execution terminated!';
  ABC_IO_EMPTY_TEXT                       = '';
  ABC_IO_DIRECTORY_DELIMITER              = '\';
  ABC_IO_WAIT_TEXT_N                      = 4;
  ABC_A_IO_WAIT_TEXT : Array [0 .. ABC_IO_WAIT_TEXT_N - 1] of String =
    (
      Chr ($08) + '-',
      Chr ($08) + '\',
      Chr ($08) + '|',
      Chr ($08) + '/'
    );
  ABC_IO_STATISTIC_TEXT_N                 = 3;
  ABC_A_IO_STATISTIC_TEXT : Array [0 .. ABC_IO_STATISTIC_TEXT_N - 1] of String =
    (
      '____________________________________________________________________',
      '',
      '____________________________________________________________________'
    );
  ABC_IO_NUMBER_OF_FILES_TEXT             = 'Number of files           : %13.0n';
  ABC_IO_TOTAL_INPUT_SIZE_TEXT            = 'Total input size          : %13.0n bytes';
  ABC_IO_TOTAL_OUTPUT_SIZE_TEXT           = 'Total output size         : %13.0n bytes';
  ABC_IO_AVERAGE_COMPRESSION_RATE_TEXT    = 'Average compression rate  : %17.3f bps';
  ABC_IO_TOTAL_COMPRESSION_RATE_TEXT      = 'Total compression rate    : %17.3f bps';
  ABC_IO_TOTAL_TIME_TEXT                  = 'Total time                : %17.3f s';


  { general data }
  ABC_CR_CHR                              = Chr ($0D);                                          { Symbol for CR }
  ABC_LF_CHR                              = Chr ($0A);                                          { Symbol for LF }
  ABC_CRLF_CHR                            = Chr ($0D) + Chr ($0A);                              { Symbol for CR/LF }
  ABC_TAB_CHR                             = Chr ($09);                                          { Symbol for tabulator }
  ABC_SPACE_CHR                           = ' ';                                                { Symbol for space }
  ABC_BYTE_BIT_SIZE                       = 8;                                                  { Number of bits per byte }
  ABC_KB                                  = 1024;                                               { 1 KB }
  ABC_BUFFER_SIZE                         = 5*1024*ABC_KB;                                      { Size of the main buffer }
  ABC_BUFFER_SAVETY_SIZE                  = ABC_BUFFER_SIZE div 16;                             { Safety area }
  ABC_ADMINISTRATION_BUFFER_SIZE          = ABC_BUFFER_SIZE div 16;                             { Size of the administration buffer }
  ABC_BUFFER_FRONT_OFFSET                 = 16;                                                 { Font buffer size for negative indices }
  ABC_BYTE_MAXIMUM_SYMBOL                 = 255;                                                { Highest byte value }
  ABC_WORD_MAXIMUM_SYMBOL                 = 65535;                                              { Highest word value }
  ABC_MOST_SIGNIFICANT_BIT                = 21+3;                                               { Highest bit for integer coding }
  ABC_BINARY_BLOCK_N                      = 64;                                                 { Number of binary blocks }
  ABC_BIT_N                               = 2;                                                  { Number of symbols for binary coding }
  ABC_BIT_DATA                            = 0;                                                  { Identifier for data bits for unary coding }
  ABC_BIT_STOP                            = 1;                                                  { Identifier for stop bit for unary coding }
  ABC_RLE_BIT_AWFC_SYMBOL_OFFSET          = 2;                                                  { Offset for AWFC symbols }
  ABC_RLE_BIT_AWFC_0A                     = 0;                                                  { Identifier for 0 for binary RLE coding }
  ABC_RLE_BIT_AWFC_0B                     = 1;                                                  { Identifier for 1 for binary RLE coding }


Type
  T_R_ABC_Commands                        = Record
                                              Help_F            : Boolean;
                                              Compress_F        : Boolean;
                                              Expand_F          : Boolean;
                                              Verbose_F         : Boolean;
                                              Fast_F            : Boolean;
                                              Input_Filename    : String;
                                              Output_Extension  : String;
                                            End; { Record }
  T_P_ABC_Longword                            = ^Longword;                                              { Pointer to longword }
  T_P_ABC_Integer                             = ^Integer;                                               { Pointer to integer }
  T_P_ABC_SmallInt                            = ^SmallInt;                                              { Pointer to slmallInt }
  T_P_ABC_Word                                = ^Word;                                                  { Pointer to word }
  T_P_ABC_Byte                                = ^Byte;                                                  { Pointer to byte }
  T_P_ABC_String                              = ^String;
  T_ABC_Byte_Array                            = Array [0 .. ABC_WORD_MAXIMUM_SYMBOL] of Byte;           { Byte array }
  T_P_ABC_Byte_Array                          = ^T_ABC_Byte_Array;                                      { Pointer to byte array }
  T_ABC_SmallInt_Array                        = Array [0 .. ABC_WORD_MAXIMUM_SYMBOL] of SmallInt;       { SmallInt array }
  T_P_ABC_SmallInt_Array                      = ^T_ABC_SmallInt_Array;                                  { Pointer to smallInt array }
  T_ABC_Word_Array                            = Array [0 .. ABC_WORD_MAXIMUM_SYMBOL] of Word;           { Word array }
  T_P_ABC_Word_Array                          = ^T_ABC_Word_Array;                                      { Pointer to Word array }
  T_ABC_Integer_Array                         = Array [0 .. ABC_WORD_MAXIMUM_SYMBOL] of Integer;        { Integer array }
  T_P_ABC_Integer_Array                       = ^T_ABC_Integer_Array;                                   { Pointer to Integer array }
  T_ABC_Longword_Array                        = Array [0 .. ABC_WORD_MAXIMUM_SYMBOL] of Longword;       { Longword array }
  T_P_ABC_Longword_Array                      = ^T_ABC_Longword_Array;                                  { Pointer to Longword array }
  T_R_ABC_IO_Block                            = Record                                                  { Record for global data block }
                                              P_Buffer    : T_P_ABC_Byte_Array;
                                              Size        : Integer;
                                            End; { T_R_ABC_IO_Block }
  T_P_R_ABC_IO_Block                          = ^T_R_ABC_IO_Block;                                      { Pointer to block record }
  T_R_ABC_Administration_Block                = Record                                                  { Record for administration data }
                                              P_Buffer    : T_P_ABC_Byte_Array;
                                              Cursor      : Integer;
                                            End; { T_R_ABC_Administration_Block }
  T_P_R_ABC_Administration_Block              = ^T_R_ABC_Administration_Block;                          { Pointer to administration record }
  T_R_ABC_Binary_Block                        = Record                                                  { Record for binary data }
                                              P_Exponent_Buffer             : T_P_ABC_Byte_Array;
                                              Exponent_Index                : Integer;
                                              A_Frequency                   : Array [0 .. ABC_MOST_SIGNIFICANT_BIT - 1] of Integer;
                                              P_Mantissa_Buffer             : T_P_ABC_Integer_Array;
                                              Mantissa_Index                : Integer;
                                              A_Mantissa_Index              : Array [0 .. ABC_MOST_SIGNIFICANT_BIT - 1] of Integer;
                                              P_RLE_Mantissa_Buffer         : T_P_ABC_Integer_Array;
                                              RLE_Mantissa_Index            : Integer;
                                              A_RLE_Frequency               : Array [0 .. ABC_MOST_SIGNIFICANT_BIT - 1] of Integer;
                                              A_RLE_Mantissa_Index          : Array [0 .. ABC_MOST_SIGNIFICANT_BIT - 1] of Integer;
                                              Bit_Index                     : Integer;
                                              A_Block_Length                : Array [0 .. ABC_BINARY_BLOCK_N] of Integer;
                                              Block_Index                   : Integer;
                                              Last_Exponent_Index           : Integer;
                                            End; { T_R_ABC_Binary_Block }
  T_P_R_ABC_Binary_Block                      = ^T_R_ABC_Binary_Block;                                  { Pointer to binary record }
  T_R_ABC_Compress_Information            = Record                                                      { Record for compress information }
                                              P_Input_Record          : T_P_R_ABC_IO_Block;             { Pointer to input record }
                                              P_Output_Record         : T_P_R_ABC_IO_Block;             { Pointer to output record }
                                              P_Administration_Block  : T_P_R_ABC_Administration_Block; { Pointer to administration record }
                                              P_Binary_Block          : T_P_R_ABC_Binary_Block;         { Pointer to binary record }
                                              P_Temp_A_0              : T_P_ABC_Integer_Array;          { Pointer to temporary integer array }
                                              P_Temp_A_1              : T_P_ABC_Integer_Array;          { Pointer to temporary integer array }
                                              Fast_F                  : Boolean;                        { Flag for fast operation }
                                              SIF_F                   : Boolean;                        { Flag for SIF }
                                              CRC32_Compress          : Longword;                       { CRC32 for data before compression }
                                              CRC32_Expand            : Longword;                       { CRC32 for data after expansion }
                                            End; { Record }


Implementation


End.

