{#####################################################################################}
{##                                                                                 ##}
{## ABC - Advanced Blocksorting Compressor                                          ##}
{##                                                                                 ##}
{## ABC_Include                                                                     ##}
{##                                                                                 ##}
{## This is the global include file                                                 ##}
{##                                                                                 ##}
{## Copyright (C) 2002-2003 J�rgen Abel                                             ##}
{##                                                                                 ##}
{##                                                                                 ##}
{##                                                                                 ##}
{## The source code is released as-is without any support.                          ##}
{## Neither warranties nor guarantees is given, that the source code is             ##}
{## free of mistakes or that it will always work perfectly.                         ##}
{## No responibility is accepted by the author for it's use or misuse of any kind.  ##}
{## Everybody, who uses the source code, is fully responsible for any damage, fees  ##}
{## or responsibilities caused by the usage.                                        ##}
{##                                                                                 ##}
{## Please note that the author is not able to give any further support for         ##}
{## used data formats, data structures, procedures, documentations or comments.     ##}
{##                                                                                 ##}
{#####################################################################################}

{$R-}                         { No range checking }
{$Q-}                         { No overflow checking }
{$A+}                         { Align records }
{$C-}                         { Assertions off }

{$DEFINE NO_WAIT}             { Don't wait after finishing }
  (* OFF ASM *)  { Use assembler optimizations }
{$DEFINE MTF0}                { Normal MTF }
{ $DEFINE MTF1}               { Delayed MTF }
{ $DEFINE MTF11}              { Delayed MTF }
{ $DEFINE SIF_NO_PERMUTATION} { No permutation at SIF }

